package com.themadguys.secondhandmarket

import kotlinx.serialization.Serializable

@Serializable
data class Item(
    var adId:String = "itemXX",
    val ownerId:String = "uID",
    var photoPath:String = "",
    val title:String = "Teddy Bear",
    val description:String = "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed do eiusmod tempor incidunt ut labore et dolore magna aliqua.",
    val price:String = "25.00",
    val category:String = "Baby: Baby & Toddler Toys",
    val location:String = "Italy",
    val expiryDate:String = "14/12/2036",
    var itemStatus:String = "ONSALE",
    var ownerNickname:String = "paolino56")