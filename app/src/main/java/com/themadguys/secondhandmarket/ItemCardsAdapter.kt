package com.themadguys.secondhandmarket

import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.themadguys.secondhandmarket.ui.boughtitemslist.BoughtItemsListFragment
import com.themadguys.secondhandmarket.ui.itemlist.ItemListFragment
import com.themadguys.secondhandmarket.ui.itemsofinterestlist.ItemsOfInterestListFragment
import com.themadguys.secondhandmarket.ui.onsalelist.OnSaleListFragment
import kotlinx.android.synthetic.main.item_card.view.*
import java.util.*
import kotlin.collections.ArrayList


class ItemCardsAdapter(private val parentFragment: Fragment) : RecyclerView.Adapter<ItemCardsAdapter.MyViewHolder>(), Filterable {

    class MyViewHolder(viewItem: View) : RecyclerView.ViewHolder(viewItem){
        val itemphoto = viewItem.itemphoto!!
        val itemtitle = viewItem.title!!
        val itemprice = viewItem.itemprice!!
        val itemlocation = viewItem.itemlocation!!
        val itemexpdate = viewItem.itemexpirationdate!!
        val editpencil = viewItem.editCard!!
        val itemstatus = viewItem.adstatus!!
        val itemowner = viewItem.itemOwner!!
    }
    private var myDataset = ArrayList<Item>()
    private var copyOfOriginalList = ArrayList<Item>(myDataset)

    fun setDataset(data : ArrayList<Item>){

            copyOfOriginalList.clear()
            copyOfOriginalList.addAll(data)
            /*diffUtil*/
            val diffCallback = ItemListDiffUtil(myDataset,data)
            val diffResult = DiffUtil.calculateDiff(diffCallback)
            diffResult.dispatchUpdatesTo(this)
            myDataset.clear()
            myDataset.addAll(data)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        // create a new view
        val viewItem = LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false)
        // set the view's size, margins, paddings and layout parameters
        return MyViewHolder(viewItem)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.itemtitle.text = myDataset[position].title
        holder.itemprice.text = myDataset[position].price
        holder.itemlocation.text = myDataset[position].location
        holder.itemexpdate.text = myDataset[position].expiryDate
        holder.itemstatus.visibility = View.GONE
        /*apply original transparent to all the card's views*/
        holder.itemtitle.alpha = 1F
        holder.itemprice.alpha = 1F
        holder.itemlocation.alpha = 1F
        holder.itemexpdate.alpha = 1F
        holder.itemowner.alpha = 1F
        holder.itemphoto.alpha = 0.40F
        holder.editpencil.alpha = 1F
        holder.itemView.currencyId.alpha = 1F
        if(myDataset[position].itemStatus != "ONSALE")
        {/*status change handling*/
            holder.itemstatus.text = myDataset[position].itemStatus
            holder.itemstatus.visibility = View.VISIBLE
            /*make more transparent all the card's views*/
            holder.itemtitle.alpha = 0.35F
            holder.itemprice.alpha = 0.35F
            holder.itemlocation.alpha = 0.35F
            holder.itemexpdate.alpha = 0.35F
            holder.itemowner.alpha = 0.35F
            holder.itemphoto.alpha = 0.35F
            holder.editpencil.alpha = 0.35F
            holder.itemView.currencyId.alpha = 0.35F
        }
        holder.itemphoto.setImageResource(R.drawable.ic_insert_photo_black_24dp)
        if(myDataset[position].photoPath != "")
            Glide.with(parentFragment.activity!!).load(myDataset[position].photoPath)
                .into(holder.itemphoto)
        val bundle = bundleOf("itemAdKey" to myDataset[position].adId)

        if(parentFragment is ItemListFragment)
        {
            holder.itemowner.visibility = View.GONE
            if(myDataset[position].itemStatus == "SOLD")
            {
                holder.editpencil.visibility = View.GONE
                holder.itemView.setOnClickListener {
                    Snackbar.make(it, "You have already sold \"${myDataset[position].title}\"", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show()
                }
            }
            else
            {
                holder.editpencil.visibility = View.VISIBLE
                holder.editpencil.setOnClickListener {
                    bundle.putString("nav", "editCard")
                    parentFragment.findNavController().navigate(R.id.action_nav_itemlist_to_itemEditFragment, bundle)
                }
                holder.itemView.setOnClickListener {
                    parentFragment.findNavController().navigate(R.id.action_nav_itemlist_to_nav_itemdetails, bundle)
                }
            }
        }
        else if(parentFragment is OnSaleListFragment)
        {
            bundle.putString("itemOwner", myDataset[position].ownerId)
            holder.editpencil.visibility = View.GONE
            holder.itemowner.text = myDataset[position].ownerNickname
            holder.itemowner.visibility = View.VISIBLE

            if(myDataset[position].itemStatus == "ONSALE")
            {
                bundle.putString("navController", "OnSaleListFragment")
                holder.itemView.setOnClickListener {
                    parentFragment.findNavController().navigate(R.id.action_nav_onSaleListFragment_to_nav_itemdetails, bundle)
                }
            }
            else
            {
                holder.itemView.setOnClickListener {
                    val message = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Html.fromHtml("This item is <b>${myDataset[position].itemStatus}</b>",0)
                    } else { //from official doc, with this surrond, deprecated tag can be ignored
                        Html.fromHtml("This item is <b>${myDataset[position].itemStatus}</b>")
                    }
                    Snackbar.make(it, message, Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show()
                }
            }
        }
        else if(parentFragment is ItemsOfInterestListFragment)
        {
            bundle.putString("itemOwner", myDataset[position].ownerId)
            holder.editpencil.visibility = View.GONE
            holder.itemowner.text = myDataset[position].ownerNickname
            holder.itemowner.visibility = View.VISIBLE

            if(myDataset[position].itemStatus == "ONSALE")
            {
                bundle.putString("navController", "ItemsOfInterestListFragment")
                holder.itemView.setOnClickListener {
                    parentFragment.findNavController().navigate(R.id.action_nav_itemsOfInterestListFragment_to_nav_itemdetails, bundle)
                }
            }
            else
            {
                holder.itemView.setOnClickListener {
                    val message = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Html.fromHtml("This item is <b>${myDataset[position].itemStatus}</b>",0)
                    } else {
                        Html.fromHtml("This item is <b>${myDataset[position].itemStatus}</b>")
                    }
                    Snackbar.make(it, message, Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show()
                }
            }
        }
        else if(parentFragment is BoughtItemsListFragment)
        {
            bundle.putString("itemOwner", myDataset[position].ownerId)
            holder.editpencil.visibility = View.GONE
            holder.itemowner.text = myDataset[position].ownerNickname
            holder.itemowner.visibility = View.VISIBLE
            holder.itemstatus.visibility = View.VISIBLE
            if(myDataset[position].itemStatus == "NOT RATED")
            {
                /*apply original transparent to all the card's views*/
                holder.itemtitle.alpha = 1F
                holder.itemprice.alpha = 1F
                holder.itemlocation.alpha = 1F
                holder.itemexpdate.alpha = 1F
                holder.itemowner.alpha = 1F
                holder.itemphoto.alpha = 0.40F
                holder.editpencil.alpha = 1F
                holder.itemView.currencyId.alpha = 1F
                bundle.putString("itemPhoto", myDataset[position].photoPath)
                holder.itemView.setOnClickListener {
                    val message = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Html.fromHtml("Rating <b>${myDataset[position].ownerNickname}</b> for \"${myDataset[position].title}\" ...",0)
                    } else {
                        Html.fromHtml("Rating <b>${myDataset[position].ownerNickname}</b> for \"${myDataset[position].title}\" ...")
                    }
                    Snackbar.make(it, message, Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show()
                    /*navigation to Rating Editor*/
                    parentFragment.findNavController().navigate(R.id.action_nav_boughtItemsListFragment_to_ratingUserAboutItemFragment, bundle)
                }
            }
            else
            {
                holder.itemView.setOnClickListener {
                    Snackbar.make(it, "\"${myDataset[position].title}\" already rated!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show()
                }
            }
        }

        holder.itemView.setOnLongClickListener {
            val message = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml("Category for \"${myDataset[position].title}\": <br><b>${myDataset[position].category}</b>",0)
            } else {
                Html.fromHtml("Category for \"${myDataset[position].title}\": <br><b>${myDataset[position].category}</b>")
            }
            Snackbar.make(it, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show()
            true
        }


    }

    override fun getFilter() : Filter {
        return filterItems
    }

    private val filterItems = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filteredList = ArrayList<Item>()

            if(constraint == null || constraint.isEmpty()) {
                filteredList.addAll(copyOfOriginalList)
            }
            else {
                val pattern = constraint.toString().toLowerCase(Locale.ROOT).trim()

                for(item:Item in copyOfOriginalList)
                {
                    if(item.title.toLowerCase(Locale.ROOT).contains(pattern) ||
                        item.location.toLowerCase(Locale.ROOT).contains(pattern) ||
                        item.ownerNickname.toLowerCase(Locale.ROOT).contains(pattern) || /*ignored for now that, when searching in my items by my name, it finds all items*/
                        item.price.toLowerCase(Locale.ROOT).contains(pattern) ||
                        item.category.toLowerCase(Locale.ROOT).contains(pattern) ||
                        item.expiryDate.toLowerCase(Locale.ROOT).contains(pattern))
                        filteredList.add(item)
                }
            }
            return FilterResults().apply {
                values = filteredList
            }
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            myDataset.clear()
            myDataset.addAll(results?.values as ArrayList<Item>)
            notifyDataSetChanged()
        }

    }

    override fun getItemCount(): Int {
        return myDataset.size
    }

}
