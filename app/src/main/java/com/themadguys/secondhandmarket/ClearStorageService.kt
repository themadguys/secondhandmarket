package com.themadguys.secondhandmarket

import android.app.job.JobParameters
import android.app.job.JobService
import android.os.Environment
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.io.File

class ClearStorageService : JobService() {

    override fun onStartJob(params: JobParameters?): Boolean {
        Log.d("ClearStorage", "Job started!")
        doBackgroundWork(params)

        return true
    }

    private fun doBackgroundWork(params: JobParameters?) {
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        CoroutineScope(IO).launch {
            clearStorageSvc(storageDir)
            Log.d("ClearStorage", "Job finished!")
            jobFinished(params, false)
        }
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        Log.d("ClearStorage", "Job cancelled before completion!")
        return true
    }

}