package com.themadguys.secondhandmarket

import android.net.ConnectivityManager
import android.net.Network
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.Query
import com.google.firebase.ktx.Firebase
import com.themadguys.secondhandmarket.repository.ItemRepo
import com.themadguys.secondhandmarket.repository.UserRepo

class SharedViewModel : ViewModel() {

    /*vars*/
    /*Network state change management*/
    var isNetworkConnected = MutableLiveData<Boolean>()
    /*User related MVVM*/
    var userKey = ""
    var tokenId = ""
    private val userRepo = UserRepo()
    var user = MutableLiveData<User>()
    var publicUser = MutableLiveData<User>()
    /*Items related MVVM*/
    var queryMyItems : Query
    var queryItems : Query
    var queryMyInterests : Query
    var queryMyBoughts : Query
    private val itemRepo = ItemRepo()
    var myItemList = MutableLiveData<ArrayList<Item>>()
    var onSaleItemList = MutableLiveData<ArrayList<Item>>()
    var myInterestsItemList = MutableLiveData<ArrayList<Item>>()
    var myBoughtsItemList = MutableLiveData<ArrayList<Item>>()
    var hintInterest = false /*hint snackbar FAB interest ItemeDetails*/


    init {
        userKey = Firebase.auth.uid!!
        isNetworkConnected.value = false //can shown just for a while at boot -> ignored
        user = userRepo.getUser() as MutableLiveData<User>
        queryMyItems = getMyItems()
        queryMyItems.addSnapshotListener { p0, _ ->
            val itemList = ArrayList<Item>()
            if(p0 != null) {
                p0.documents.forEach {
                    it.toObject(Item::class.java)?.let { it1 -> itemList.add(it1) }
                }
                myItemList.value = itemList
            }
        }
        queryItems = getItems()
        queryItems.addSnapshotListener { p0, _ ->
            val itemList = ArrayList<Item>()
            if(p0 != null) {
                p0.documents.forEach {
                    it.toObject(Item::class.java)?.let { it1 ->
                        if(userKey != it1.ownerId)
                            itemList.add(it1)
                    }
                }
                onSaleItemList.value = itemList
            }
        }
        queryMyInterests = getMyInterests()
        queryMyInterests.addSnapshotListener { p0, _ ->
            val itemList = ArrayList<Item>()
            if(p0 != null) {
                p0.documents.forEach {
                    it.toObject(Item::class.java)?.let { it1 ->
                        if(userKey != it1.ownerId)
                            itemList.add(it1)
                    }
                }
                myInterestsItemList.value = itemList
            }
        }
        queryMyBoughts = getMyBoughts()
        queryMyBoughts.addSnapshotListener { p0, _ ->
            val itemList = ArrayList<Item>()
            if(p0 != null) {
                p0.documents.forEach {
                    it.toObject(Item::class.java)?.let { it1 ->
                        if(userKey != it1.ownerId)
                            itemList.add(it1)
                    }
                }
                myBoughtsItemList.value = itemList
            }
        }

    }

    /*Network state change management*/
    val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network?) {
            isNetworkConnected.postValue(true)
            //Log.d("google_sign", "internet connection = ${isNetworkConnected.value}")
        }

        override fun onLost(network: Network?) {
            isNetworkConnected.postValue(false)
            //Log.d("google_sign", "internet connection = ${isNetworkConnected.value}")
        }
    }

    /*User related MVVM*/
    fun putUser(){
        Log.d("firestore","Calling repo...")
        user.value?.let {
            Log.d("firestore","inside let.. with ${user.value}")
            userRepo.putUser(it) }
    }

    fun getUser(): LiveData<User> {
        return user
    }

    /*Items related MVVM*/
    private fun getMyItems() : Query {
        return itemRepo.getMyItems()
    }

    private fun getItems() : Query {
        return itemRepo.getItems()
    }

    private fun getMyInterests() : Query {
        return itemRepo.getMyInterests()
    }

    private fun getMyBoughts() : Query {
        return itemRepo.getMyBoughts()
    }

    fun getMyItem(adId : String) : Item {
        /*public to use it in a static way in the Editor*/
        var myItem : Item? = null
        myItemList.value!!.forEach { /*con Coroutine!!!*/
            if(it.adId == adId)
                myItem = it
        }
        return myItem!!
    }

    fun setItem(item : Item, buyer : User?, notifyUsers : Boolean) {
        itemRepo.setMyItem(item, buyer, notifyUsers) /*when my item is ready in my list, it will be set to onSale*/
    }

    fun showPublicUser(uId : String) {
        userRepo.showUser(uId)
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    publicUser.value = task.result!!.toObject(User::class.java)
                } else {
                    Log.d("firestore","get KO!")
                }
            }
    }

    fun removeAllMyInterests() {
        myInterestsItemList.value?.forEach {
            itemRepo.removeInterestedUser(it.adId,it.ownerId)
        }
    }

    fun putRating(myRating: Rating, sellerId : String, itemId : String) {
        /*set a new rating to users/{uId}/myratings*/
        userRepo.putRating(myRating, sellerId, itemId)
    }
}