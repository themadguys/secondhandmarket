package com.themadguys.secondhandmarket

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        // There are two types of messages: data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated ams notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("pushMessage", "From: ${remoteMessage.from}")

        // Check if message contains a data payload.
        remoteMessage.data.isNotEmpty().let {
            Log.d("pushMessage", "Message data payload: " + remoteMessage.data)
            val body = remoteMessage.data["body"] as String
            val title = remoteMessage.data["title"] as String
            val picPath = remoteMessage.data["picPath"] as String
            val pushId = remoteMessage.data["pushId"] as String
            sendNotification(body, title, picPath, pushId.toInt())
            /*if (*//* Check if data needs to be processed by long running job *//* true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                //scheduleJob()
            } else {
                // Handle message within 10 seconds
                Log.d("pushMessage", "Short lived task is done.")
            }*/
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d("pushMessage", "Message Notification Body: ${it.body}")
            //sendNotification(it.body!!, it.title!!)
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    override fun onMessageSent(message: String) {
        super.onMessageSent(message)
    }

    override fun onNewToken(token: String) {
        Log.d("FCMtoken", "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(token)

    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageBody: String, messageTitle: String, largeIconPath: String, pushId: Int) {
        val intent = Intent(this, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT)

        var itemPic = Glide.with(this).asBitmap().load(R.drawable.ic_insert_photo_black_24dp).submit(100,100).get()
        if(largeIconPath != "")
            itemPic = Glide.with(this).asBitmap().load(largeIconPath).submit(100,100).get()
        val channelId = getString(R.string.consumer_notification_channel_id)
        val channelName = getString(R.string.consumer_notification_channel_name)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_push_icon)
            .setLargeIcon(itemPic)
            .setColor(ContextCompat.getColor(this, R.color.colorAccent))
            .setColorized(true)
            .setContentTitle(/*getString(R.string.push_title)*/messageTitle)
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
            .setLights(Color.YELLOW,1000,1000)
            //.setStyle(androidx.media.app.NotificationCompat.MediaStyle())
            .setStyle(NotificationCompat.BigPictureStyle()
                .bigPicture(itemPic)
                .bigLargeIcon(null))

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT)
            channel.enableLights(true)
            channel.lightColor = Color.YELLOW
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(pushId, notificationBuilder.build())
    }

}