package com.themadguys.secondhandmarket

import kotlinx.serialization.Serializable

@Serializable
data class Rating(
    var buyerPhotoPath: String = "",
    val title: String = "Awesome seller!!",
    val comment: String = "",
    val score: String = "2.6"//,
    //var buyerId: String = ""
)