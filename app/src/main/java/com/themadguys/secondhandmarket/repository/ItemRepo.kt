package com.themadguys.secondhandmarket.repository

import android.net.Uri
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.storage.FirebaseStorage
import com.themadguys.secondhandmarket.Item
import com.themadguys.secondhandmarket.User

class ItemRepo {

    private val db = FirebaseFirestore.getInstance()
    private var userAuth = FirebaseAuth.getInstance().currentUser
    private val myDocRef = db.collection("users").document(userAuth!!.uid).collection("myitems")
    private val onSaleDocRef = db.collection("itemsonsale")
    private val notificationsRef = db.collection("notifications")
    private val mStorageRef = FirebaseStorage.getInstance().reference
    private val picRef = mStorageRef.child(userAuth!!.uid + "/myitems")

    fun getMyItems(): Query {
        return myDocRef.orderBy("expiryDate", Query.Direction.DESCENDING)
            .orderBy("title", Query.Direction.ASCENDING) /*check queries*/
    }

    fun getItems(): Query {
        return onSaleDocRef.orderBy("expiryDate", Query.Direction.DESCENDING)
            .orderBy("title", Query.Direction.ASCENDING) /*check queries*/
    }

    fun setMyItem(item: Item, buyer : User?, notifyUsers : Boolean) {
        /*save item to my list*/
        var adId = item.adId
        if (adId == "") {
            adId = myDocRef.document().id
            item.adId = adId
        }
        if (item.photoPath.contains("itemPic_")) {/*if there's a new pic*/
            val ref = picRef.child("$adId.jpg")
            ref.putFile(Uri.parse(item.photoPath))
                .addOnSuccessListener {
                    ref.downloadUrl
                        .addOnSuccessListener {
                            item.photoPath = it.toString()
                            myDocRef.document(adId)
                                .set(item)
                                .addOnSuccessListener {
                                    /*now that's ready we publish to onSale list*/
                                    setItem(item,buyer,notifyUsers)
                                    Log.d("firestore", "put OK!")
                                }
                                .addOnFailureListener {
                                    Log.d("firestore", "put KO!")
                                }
                            Log.d("firestorage", "put OK!\nwith ${Uri.parse(item.photoPath)}")
                        }
                }
                .addOnFailureListener {
                    Log.d("firestorage", "put KO!\nwith ${Uri.parse(item.photoPath)}")
                }
        } else {
            myDocRef.document(adId)
                .set(item)
                .addOnSuccessListener {
                    /*now that's ready we publish to onSale list*/
                    setItem(item,buyer,notifyUsers)
                    Log.d("firestore", "put OK!")
                }
                .addOnFailureListener {
                    Log.d("firestore", "put KO!")
                }
        }


    }

    fun getItem(adId: String): DocumentReference {
        return myDocRef.document(adId)
    }

    fun getItemOnSale(adId: String): DocumentReference {
        return onSaleDocRef.document(adId)
    }

    private fun setItem(item: Item, buyer: User?, notifyUsers: Boolean) { /*onSale*/
        /*save item to onSale list*/
        onSaleDocRef.document(userAuth!!.uid + "_" + item.adId)
            .set(item)
            .addOnSuccessListener {
                Log.d("firestore", "put OK!")

                if(notifyUsers)
                {
                    /*create notification that admin server will handle*/
                    val interestedUsersTokens = ArrayList<String>()
                    getMyItemInterestedUsers(item.adId)
                        .get()
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                task.result!!.documents.forEach {
                                    it.toObject(User::class.java)?.let { it1 ->
                                        if(buyer != null)
                                        {/*if there is a buyer, send him/her a different notification*/
                                            if(it1.userId != buyer.userId)
                                                interestedUsersTokens.add(it1.tokenId)
                                            return@let
                                        }
                                        else
                                        {
                                            interestedUsersTokens.add(it1.tokenId)
                                        }
                                    }
                                }
                                if(item.itemStatus == "SOLD" && buyer != null)
                                {
                                    /*notify all interested users*/
                                    val notificationPayload = HashMap<String,String>()
                                    notificationPayload["destToken"] = interestedUsersTokens.joinToString(", ")
                                    notificationPayload["fromName"] = ""
                                    notificationPayload["itemTitle"] = item.title
                                    notificationPayload["type"] = "ITEM_SOLD"
                                    notificationPayload["itemPic"] = item.photoPath
                                    notificationsRef.document()
                                        .set(notificationPayload)
                                    /*notify the buyer*/
                                    val notificationPayload2 = HashMap<String,String>()
                                    notificationPayload2["destToken"] = buyer.tokenId
                                    notificationPayload2["fromName"] = buyer.userId + "_" + item.ownerId + "_" + item.adId
                                    notificationPayload2["itemTitle"] = item.title
                                    notificationPayload2["type"] = "ITEM_SOLD_TO_YOU"
                                    notificationPayload2["itemPic"] = item.photoPath
                                    notificationsRef.document()
                                        .set(notificationPayload2)
                                }
                                else if(item.itemStatus == "UNAVAILABLE")
                                {
                                    val notificationPayload = HashMap<String,String>()
                                    notificationPayload["destToken"] = interestedUsersTokens.joinToString(", ")
                                    notificationPayload["fromName"] = ""
                                    notificationPayload["itemTitle"] = item.title
                                    notificationPayload["type"] = "ITEM_BLOCKED"
                                    notificationPayload["itemPic"] = item.photoPath
                                    notificationsRef.document()
                                        .set(notificationPayload)
                                }
                            }
                        }
                }

            }
            .addOnFailureListener {
                Log.d("firestore", "put KO!")
            }
    }

    fun addInterestedUser(thisItem : Item, user : User) {

        val ownerRef = db.collection("users")
            .document(thisItem.ownerId).collection("myitems")
            .document(thisItem.adId).collection("interestedusers")
        ownerRef.document(userAuth!!.uid).set(user)
            .addOnSuccessListener {
                /*val result = HashMap<String,String>()
                result["itemStatus"] = "ONSALE"
                result["notified"] = "true" *//*in case of push notifications or for a new fragment MyInterests*/
                db.collection("users").document(userAuth!!.uid)
                    .collection("myinterests")
                    .document(thisItem.ownerId + "_" + thisItem.adId)
                    .set(thisItem)

                /*create notification that admin server will handle
                * it could be more simple, passing only the IDs to the server
                * that would do the rest, but it's written here only for clear reading*/
                db.collection("users").document(thisItem.ownerId)
                    .get()
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val destUser = task.result!!.toObject(User::class.java)
                            val notificationPayload = HashMap<String,String>()
                            notificationPayload["destToken"] = destUser!!.tokenId
                            notificationPayload["fromName"] = user.nickname
                            notificationPayload["itemTitle"] = thisItem.title
                            notificationPayload["type"] = "NEW_INTEREST"
                            notificationPayload["itemPic"] = thisItem.photoPath
                            notificationsRef.document()
                                .set(notificationPayload)
                        }
                    }
            }
    }

    fun removeInterestedUser(adId: String, ownerId: String) {
        val ownerRef = db.collection("users")
            .document(ownerId).collection("myitems")
            .document(adId).collection("interestedusers")
        ownerRef.document(userAuth!!.uid).delete()
            .addOnSuccessListener {
                db.collection("users").document(userAuth!!.uid)
                    .collection("myinterests")
                    .document(ownerId + "_" + adId)
                    .delete()
            }
    }

    fun isInterestedUser(adId: String, ownerId: String): CollectionReference {

        return db.collection("users")
            .document(ownerId).collection("myitems")
            .document(adId).collection("interestedusers")
    }

    fun getMyItemInterestedUsers(adId : String) : Query{
        return myDocRef.document(adId).collection("interestedusers")
    }

    fun getMyInterests() : Query {
        return db.collection("users").document(userAuth!!.uid)
            .collection("myinterests").orderBy("expiryDate", Query.Direction.DESCENDING)
            .orderBy("title", Query.Direction.ASCENDING)
    }

    fun getMyBoughts(): Query {
        return db.collection("users").document(userAuth!!.uid)
            .collection("myboughts")
            .orderBy("itemStatus", Query.Direction.ASCENDING)
            .orderBy("expiryDate", Query.Direction.DESCENDING)
            .orderBy("title", Query.Direction.ASCENDING)
    }


}