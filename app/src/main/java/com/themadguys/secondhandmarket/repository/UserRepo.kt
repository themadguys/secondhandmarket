package com.themadguys.secondhandmarket.repository

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.themadguys.secondhandmarket.Item
import com.themadguys.secondhandmarket.Rating
import com.themadguys.secondhandmarket.User
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class UserRepo {

    private val db = FirebaseFirestore.getInstance()
    private var userAuth = FirebaseAuth.getInstance().currentUser
    private val mStorageRef = FirebaseStorage.getInstance().reference
    private val picRef = mStorageRef.child(userAuth!!.uid + "/profile.jpg")
    private val itemRepo = ItemRepo()

    fun getUser() : LiveData<User> {
        val info = MutableLiveData<User>()

        db.collection("lastDbAccess").document("access")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val lastDate = task.result!!.getString("date")!!
                    val currentDate = SimpleDateFormat("yyyy/MM/dd", Locale.ITALY).format(Date())

                    if(currentDate > lastDate)
                    {
                        val newAccess = HashMap<String,String>()
                        newAccess["date"] = currentDate
                        db.collection("lastDbAccess").document("access")
                            .set(newAccess)
                    }

                }
            }

        db.collection("users").document(userAuth!!.uid)
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    val data = task.result!!.toObject(User::class.java)
                    if(data == null)
                    {/*at first account access we create the document in the db*/
                        info.value = User(name = userAuth!!.displayName!!,
                            email = userAuth!!.email!!,
                        photoPath = userAuth!!.photoUrl!!.toString())
                        putUser(info.value!!)
                    }
                    else
                        info.value = data
                    Log.d("firestore","get OK!\n ${info.value}\n\n")


                } else {
                    Log.d("firestore","get KO!")
                }
            }

        return info
    }

    fun putUser(user : User){

        if (user.photoPath.contains("profilePic")) {/*if there's a new pic*/

            picRef.putFile(Uri.parse(user.photoPath))
                .addOnSuccessListener {
                    picRef.downloadUrl
                        .addOnSuccessListener {
                            user.photoPath = it.toString()
                            db.collection("users").document(userAuth!!.uid)
                                .set(user)
                                .addOnSuccessListener {
                                    Log.d("firestore", "put OK!")
                                    updateUserItems(user)
                                    updateUserInterestedItems(user)
                                }
                                .addOnFailureListener {
                                    Log.d("firestore", "put KO!")
                                }
                        }
                }
                .addOnFailureListener {
                    Log.d("firestorage", "put KO!")
                }
        } else {
            db.collection("users").document(userAuth!!.uid)
                .set(user)
                .addOnSuccessListener {
                    Log.d("firestore", "put OK!")
                    updateUserItems(user)
                    updateUserInterestedItems(user)
                }
                .addOnFailureListener {
                    Log.d("firestore", "put KO!")
                }
        }

    }

    fun showUser(uId : String) : DocumentReference{
        return db.collection("users").document(uId)
    }

    private fun updateUserItems(user : User) {
        itemRepo.getMyItems()
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    var item : Item
                    task.result!!.documents.forEach {
                        item = it.toObject(Item::class.java)!!
                        item.ownerNickname = user.nickname
                        /*and other things*/
                        if(item.itemStatus != "SOLD")
                            itemRepo.setMyItem(item,null,false)
                    }

                } else {
                    Log.d("firestore","update KO!")
                }
            }
    }

    private fun updateUserInterestedItems(user : User) {
        /*update user info saved in each interested item of another user*/
        db.collection("users").document(userAuth!!.uid)
            .collection("myinterests").get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    task.result!!.documents.forEach {
                        val ownerId = it.id.split("_")[0]
                        val adId = it.id.split("_")[1]
                        db.collection("users")
                            .document(ownerId).collection("myitems")
                            .document(adId).collection("interestedusers")
                            .document(userAuth!!.uid).set(user)
                    }
                }

            }

    }

    fun putRating(myRating: Rating, sellerId: String, itemId : String) {
        showUser(sellerId).collection("myratings")
            .document()
            .set(myRating)
            .addOnSuccessListener {
                Log.d("firestore", "put Rating OK!")
                /*change my bought item to RATED*/
                db.collection("users").document(userAuth!!.uid)
                    .collection("myboughts")
                    .document(sellerId + "_" + itemId)
                    .get()
                    .addOnCompleteListener { task ->
                        if(task.isSuccessful) {
                            val ratedItem = task.result!!.toObject(Item::class.java)
                            ratedItem!!.itemStatus = "RATED"
                            db.collection("users").document(userAuth!!.uid)
                                .collection("myboughts")
                                .document(sellerId + "_" + itemId)
                                .set(ratedItem)
                        }
                    }
                /*send notification to seller*/
                notifySeller(myRating,sellerId)
            }
            .addOnFailureListener {
                Log.d("firestore", "put Rating FAILED!")
            }
    }

    private fun notifySeller(myRating: Rating, sellerId: String) {

        db.collection("users").document(sellerId)
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val destUser = task.result!!.toObject(User::class.java)
                    val notificationPayload = HashMap<String,String>()
                    notificationPayload["destToken"] = destUser!!.tokenId
                    notificationPayload["fromName"] = ""
                    notificationPayload["itemTitle"] = ""
                    notificationPayload["type"] = "RATED"
                    notificationPayload["itemPic"] = myRating.buyerPhotoPath

                    db.collection("notifications").document()
                        .set(notificationPayload)
                }
            }
    }

    fun getUserRatings(userId: String): CollectionReference {
        return showUser(userId).collection("myratings")
    }

}