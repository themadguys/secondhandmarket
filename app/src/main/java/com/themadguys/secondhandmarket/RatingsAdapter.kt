package com.themadguys.secondhandmarket

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.rating_card.view.*

class RatingsAdapter(private val parentFragment : Fragment) : RecyclerView.Adapter<RatingsAdapter.MyViewHolder>() {
    class MyViewHolder(viewItem: View) : RecyclerView.ViewHolder(viewItem){
        val userphoto = viewItem.photo
        val ratingTitle = viewItem.title
        val ratingComment = viewItem.rating_comment
        val ratingScore = viewItem.rating_score
    }

    private var myDataset = ArrayList<Rating>()

    fun setDataset(data : ArrayList<Rating>){

        myDataset.clear()
        myDataset.addAll(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val viewItem = LayoutInflater.from(parent.context).inflate(R.layout.rating_card, parent, false)
        return MyViewHolder(viewItem)
    }

    override fun getItemCount(): Int {
        return myDataset.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.userphoto.setImageResource(R.drawable.ic_person_black_24dp)
        if(myDataset[position].buyerPhotoPath != "")
            Glide.with(parentFragment.activity!!).load(myDataset[position].buyerPhotoPath)
                .circleCrop().into(holder.userphoto)
        holder.ratingTitle.text = myDataset[position].title
        holder.ratingComment.text = myDataset[position].comment
        holder.ratingScore.rating = myDataset[position].score.toFloat()

    }
}