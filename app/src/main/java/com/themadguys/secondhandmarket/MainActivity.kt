package com.themadguys.secondhandmarket

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.*
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.*
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private var host: NavHostFragment? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var viewModel: SharedViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        host = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = host?.navController

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_showprofile,
                R.id.nav_itemlist,
                R.id.nav_onSaleListFragment,
                R.id.nav_itemsOfInterestListFragment,
                R.id.nav_boughtItemsListFragment
            ), drawerLayout
        )
        setupActionBarWithNavController(navController!!, appBarConfiguration)
        navView.setupWithNavController(navController)

        auth = Firebase.auth
        updateUI(auth.currentUser)

        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_logout -> signOut()
                else -> NavigationUI.onNavDestinationSelected(it, navController)
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }


        val alert = AlertDialog.Builder(this).setTitle("No internet connection")
            .setMessage("Please connect to a network to use all the app functionalities")
            .setPositiveButton("Ok, understood.") { dialog: DialogInterface, _: Int ->
                dialog.dismiss()
            }.setIcon(R.drawable.ic_warning_black_24dp).setCancelable(false).create()
        viewModel.isNetworkConnected.observe(this, Observer {

            if (it == false) {
                alert.show()
            } else
                alert.cancel()

        })
        registerNetworkCallback(this) /*set a viewModel var with the status
                                       * of current internet connection*/

    }

    private fun signOut() {
        AlertDialog.Builder(this).setTitle("Log out").setMessage("Are you sure?")
            .setPositiveButton("Yes") { dialog: DialogInterface, _: Int ->
                dialog.dismiss()
                val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build()
                val googleSignInClient = GoogleSignIn.getClient(this, gso)
                /*also delete the FCM token so it can be newly generated again at next login*/
                CoroutineScope(Default).launch {
                    FirebaseInstanceId.getInstance().deleteInstanceId()
                }
                auth.signOut()
                googleSignInClient.signOut().addOnCompleteListener(this) {
                    updateUI(null)
                }
            }.setNegativeButton("No") { dialog: DialogInterface, _: Int ->
            dialog.cancel()
        }.create().show()
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            Log.d(
                "google_sign", "name = ${user.displayName}\n" +
                        "mail = ${user.email}\n" +
                        "photo = ${user.photoUrl}"
            )
            viewModel = ViewModelProviders.of(this).get(SharedViewModel::class.java)
            viewModel.getUser().observe(this, Observer {

                CoroutineScope(Main).launch {
                    if (it.photoPath != "")
                        Glide.with(this@MainActivity).load(it.photoPath)
                            .circleCrop().into(nav_view.getHeaderView(0).header_userpic)
                }

                nav_view.getHeaderView(0).header_username.text = it.name
                nav_view.getHeaderView(0).header_usermail.text = it.email

                if (it.userId == "profileNew") {/*welcome msg at first access -> no info saved*/
                    AlertDialog.Builder(this).setTitle(getString(R.string.app_name))
                        .setMessage("Welcome ${user.displayName}!\n\nYou can now modify your profile information from the left menu.\n\n\nEnjoy our app!")
                        .setPositiveButton("Ok, don't show again.") { dialog: DialogInterface, _: Int ->
                            dialog.dismiss()
                            viewModel.user.value = User(
                                userId = user.uid,
                                name = user.displayName!!,
                                email = user.email!!,
                                photoPath = user.photoUrl!!.toString()
                            )
                            viewModel.putUser() /*dismiss forever this alert*/
                        }.setCancelable(false).create().show()
                }

                sendNotificationToken(it)
            })
        } else {
            Log.w("google_sign", "must be logged in!!! exiting...")
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


    // Network Check
    private fun registerNetworkCallback(context: Context) {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkCallback = viewModel.networkCallback
        try {
            connectivityManager.unregisterNetworkCallback(networkCallback)
        } catch (e: Exception) {
            Log.w(
                "google_sign",
                "NetworkCallback was not registered or already unregistered"
            )
        }

        try {

            val builder = NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                .build()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                connectivityManager.registerDefaultNetworkCallback(networkCallback)
            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                connectivityManager.registerNetworkCallback(builder, networkCallback)
            else {/*must be listen for...ignored for API < 23*/
                val activeNetwork = connectivityManager.activeNetworkInfo
                val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
                Log.d("google_sign", "API < 24 : Internet connection = $isConnected")
                viewModel.isNetworkConnected.value = isConnected
            }

        } catch (e: Exception) {
            Log.d("google_sign", "Internet connection = $e")
            viewModel.isNetworkConnected.value = false
        }
    }

    /*FCM token instance*/
    fun sendNotificationToken(user : User) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener{ task ->
                if (!task.isSuccessful) {
                    Log.w("FCMtoken", "getInstanceId failed", task.exception)
                    return@addOnCompleteListener
                }
                // Get new Instance ID token
                viewModel.tokenId = task.result?.token!! // !! o con il let??!
                Log.d("FCMtoken", "InstanceID Token: ${viewModel.tokenId}")
                user.tokenId = viewModel.tokenId
                viewModel.putUser()
            }
    }

}
