package com.themadguys.secondhandmarket.ui.onsalelist

import android.content.res.Configuration
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.PopupMenu
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.themadguys.secondhandmarket.ItemCardsAdapter
import com.themadguys.secondhandmarket.R
import com.themadguys.secondhandmarket.SharedViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_list_fragment.*
import kotlinx.coroutines.*

class OnSaleListFragment : Fragment() {

    companion object {
        fun newInstance() = OnSaleListFragment()
    }

    private lateinit var viewModel: SharedViewModel
    private var backPressedTime : Long = 0
    private lateinit var backToast : Toast
    private lateinit var mAdapter : ItemCardsAdapter
    private lateinit var viewManagerPortrait : LinearLayoutManager
    private lateinit var viewManagerLandscape : GridLayoutManager
    private var searchText = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.on_sale_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navView: NavigationView = activity!!.findViewById(R.id.nav_view)
        navView.setCheckedItem(R.id.nav_onSaleListFragment)

        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity!!.let { viewModel = ViewModelProviders.of(it).get(SharedViewModel::class.java) }

        viewManagerPortrait = LinearLayoutManager(activity)
        viewManagerLandscape = GridLayoutManager(activity, 3)

        mAdapter = ItemCardsAdapter(this)
        listItems.apply {
            setHasFixedSize(true)
            // use a linear layout manager if portrait, grid one else
            layoutManager =
                if (activity!!.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
                    viewManagerLandscape
                else
                    viewManagerPortrait
            adapter = mAdapter
            itemAnimator = DefaultItemAnimator()
        }

        viewModel.onSaleItemList.observe(viewLifecycleOwner, Observer {
            if(it.isEmpty()) {
                emptyAds.visibility = View.VISIBLE
                listItems.visibility = View.GONE
            }
            else {
                emptyAds.visibility = View.GONE
                listItems.visibility = View.VISIBLE
            }
            loading_ads.visibility = View.VISIBLE
            CoroutineScope(Dispatchers.Default).launch {
                mAdapter.setDataset(it)
                withContext(Dispatchers.Main){
                    loading_ads.visibility = View.GONE
                }
            }
        })

        if(savedInstanceState != null && savedInstanceState.containsKey("searchText"))
            searchText = savedInstanceState.getString("searchText")!!
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if(searchText != "")
            outState.putString("searchText", searchText)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.category_filter, menu)
        inflater.inflate(R.menu.search_filter, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.maxWidth = Integer.MAX_VALUE
        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
        searchView.queryHint = "Search by item's title, location, price, expiration date..."
        if(searchText != "") /*ignored when there are incoming updates from db, at rotation query takes place*/
        {
            menu.findItem(R.id.action_search).expandActionView()
            searchView.post { searchView.setQuery(searchText,false) }
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener
        {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                /*perform filtering*/
                Log.d("myItems", "filtering by $newText")
                searchText = newText /*searchView state management*/

                mAdapter.filter.filter(newText)
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.action_filter -> {
                val viewFilter = activity?.findViewById<View>(R.id.action_filter)
                val popup = PopupMenu(activity,viewFilter)
                popup.inflate(R.menu.filter_cat_list)
                val clearTitle = SpannableString(popup.menu.findItem(R.id.clear_filter).title)
                    .apply {
                        setSpan(ForegroundColorSpan(ContextCompat.getColor(activity!!, R.color.colorPrimaryDark)),0,length,0)
                    }
                popup.menu.findItem(R.id.clear_filter).title = clearTitle
                popup.setOnMenuItemClickListener { itemcat ->
                    if(itemcat.itemId == R.id.clear_filter)
                    {
                        Toast.makeText(activity, "Filter cleared", Toast.LENGTH_SHORT).show()
                        mAdapter.filter.filter("")
                    }
                    else
                    {
                        /*ignored group title -> parent category*/
                        Toast.makeText(activity, "Filtering by ${itemcat.title}", Toast.LENGTH_SHORT).show()
                        mAdapter.filter.filter(itemcat.title)
                    }

                    true
                }
                popup.show()

                true
            }
            R.id.action_search -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if(activity!!.drawer_layout.isDrawerOpen(GravityCompat.START))
                    activity!!.drawer_layout.closeDrawers()
                else
                {
                    if(backPressedTime + 2000 > System.currentTimeMillis()) /*user has to click again in 2 seconds*/
                    {
                        backToast.cancel() /*to delete toast on app close*/
                        activity!!.finish()
                    }
                    else
                    {
                        backToast = Toast.makeText(activity, "Press again to close app", Toast.LENGTH_SHORT)
                        backToast.show()
                    }
                    backPressedTime = System.currentTimeMillis()
                }
            }
        })
    }

}
