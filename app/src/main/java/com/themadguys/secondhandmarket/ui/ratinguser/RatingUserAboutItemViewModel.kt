package com.themadguys.secondhandmarket.ui.ratinguser

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.themadguys.secondhandmarket.User
import com.themadguys.secondhandmarket.repository.UserRepo

class RatingUserAboutItemViewModel : ViewModel() {

    lateinit var sellerId : String
    lateinit var itemId: String
    var sellerPhoto = MutableLiveData<String>()
    var itemPhoto = ""

    private val userRepo = UserRepo()

    fun getSellerPhoto(sellerId : String) {
        userRepo.showUser(sellerId)
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val seller = task.result!!.toObject(User::class.java)
                    sellerPhoto.value = seller!!.photoPath
                }
            }
    }
}
