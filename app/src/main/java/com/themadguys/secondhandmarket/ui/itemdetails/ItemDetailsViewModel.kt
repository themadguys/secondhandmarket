package com.themadguys.secondhandmarket.ui.itemdetails

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.themadguys.secondhandmarket.Item
import com.themadguys.secondhandmarket.Rating
import com.themadguys.secondhandmarket.User
import com.themadguys.secondhandmarket.repository.ItemRepo
import com.themadguys.secondhandmarket.repository.UserRepo

class ItemDetailsViewModel : ViewModel() {

    private val itemRepo = ItemRepo()
    private val userRepo = UserRepo()
    private var userKey = FirebaseAuth.getInstance().currentUser!!.uid

    var currentItem = MutableLiveData<Item>()
    var isInterested = MutableLiveData<Boolean>()
    var interestedUsersList = MutableLiveData<ArrayList<User>>()
    var sellerPhoto = MutableLiveData<String>()
    var ratingsList = MutableLiveData<ArrayList<Rating>>()


    fun getItem(adId : String, ownerId : String) {
        if(ownerId == userKey)
            itemRepo.getItem(adId)
                .addSnapshotListener { documentSnapshot, _ ->
                    if(documentSnapshot != null)
                        currentItem.value = documentSnapshot.toObject(Item::class.java)
                }
        else
            itemRepo.getItemOnSale(ownerId + "_" + adId)
                .addSnapshotListener { documentSnapshot, _ ->
                    if(documentSnapshot != null)
                        currentItem.value = documentSnapshot.toObject(Item::class.java)
                }
    }

    fun addInterestedUser(thisItem: Item, thisUser : User) {
        itemRepo.addInterestedUser(thisItem,thisUser)
    }

    fun removeInterestedUser(adId : String, ownerId : String) {
        itemRepo.removeInterestedUser(adId,ownerId)
    }

    fun isInterested(adId : String, ownerId : String) {

        itemRepo.isInterestedUser(adId,ownerId)
            .addSnapshotListener { querySnapshot, _ ->
                if(querySnapshot != null)
                {
                    var result = false
                    querySnapshot.documents.forEach {
                        if(it != null)
                            if(it.id == userKey)
                                result = true
                    }
                    isInterested.value = result
                }

            }
    }

    fun getMyItemInterestedUsers(adId : String) {
        val query = itemRepo.getMyItemInterestedUsers(adId)
        query.addSnapshotListener { p0, _ ->
            val userList = ArrayList<User>()
            if(p0 != null) {
                p0.documents.forEach {
                    it.toObject(User::class.java)?.let { it1 ->
                        userList.add(it1)
                    }
                }
                interestedUsersList.value = userList
            }
        }
    }

    fun getSellerPhoto(sellerId : String) {
        userRepo.showUser(sellerId)
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val seller = task.result!!.toObject(User::class.java)
                    sellerPhoto.value = seller!!.photoPath
                }
            }
    }

    fun getRatings(userId : String) {
        val query = userRepo.getUserRatings(userId)
        query.addSnapshotListener { p0, _ ->
            val userList = ArrayList<Rating>()
            if(p0 != null) {
                p0.documents.forEach {
                    it.toObject(Rating::class.java)?.let { it1 ->
                        userList.add(it1)
                    }
                }
                ratingsList.value = userList
            }
        }
    }
}
