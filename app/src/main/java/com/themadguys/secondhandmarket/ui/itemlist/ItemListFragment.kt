package com.themadguys.secondhandmarket.ui.itemlist

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.SearchView
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.themadguys.secondhandmarket.ItemCardsAdapter
import com.themadguys.secondhandmarket.R
import com.google.android.material.snackbar.Snackbar
import com.themadguys.secondhandmarket.SharedViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_list_fragment.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main

class ItemListFragment : Fragment() {

    companion object {
        fun newInstance() = ItemListFragment()
    }

    private lateinit var viewModel: SharedViewModel
    private lateinit var mAdapter : ItemCardsAdapter
    private lateinit var viewManagerPortrait : LinearLayoutManager
    private lateinit var viewManagerLandscape : GridLayoutManager
    private var searchText = ""

    @SuppressLint("RestrictedApi")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.item_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fab_addItem.setOnClickListener {

            Snackbar.make(it, "Creating new advertisement...", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            findNavController().navigate(R.id.action_nav_itemlist_to_itemEditFragment)

        }

        setHasOptionsMenu(true)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity!!.let { viewModel = ViewModelProviders.of(it).get(SharedViewModel::class.java) }

        viewManagerPortrait = LinearLayoutManager(activity)
        viewManagerLandscape = GridLayoutManager(activity, 3)

        mAdapter = ItemCardsAdapter(this)
        listItems.apply {
            setHasFixedSize(true)
            // use a linear layout manager if portrait, grid one else
            layoutManager =
                if (activity!!.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
                    viewManagerLandscape
                else
                    viewManagerPortrait
            adapter = mAdapter
            itemAnimator = DefaultItemAnimator()
        }

        viewModel.myItemList.observe(viewLifecycleOwner, Observer {
            if(it.isEmpty()) {
                emptyAds.visibility = View.VISIBLE
                listItems.visibility = View.GONE
            }
            else {
                emptyAds.visibility = View.GONE
                listItems.visibility = View.VISIBLE
            }
            loading_ads.visibility = View.VISIBLE
            CoroutineScope(Default).launch {
                mAdapter.setDataset(it)
                withContext(Main){
                    loading_ads.visibility = View.GONE
                }
            }
        })

        if(savedInstanceState != null && savedInstanceState.containsKey("searchText"))
            searchText = savedInstanceState.getString("searchText")!!

    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if(activity!!.drawer_layout.isDrawerOpen(GravityCompat.START))
                    activity!!.drawer_layout.closeDrawers()
                else
                    findNavController().navigateUp()
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if(searchText != "")
            outState.putString("searchText", searchText)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_filter, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.maxWidth = Integer.MAX_VALUE
        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
        searchView.queryHint = "Search by item's title, location, price, expiration date..."
        if(searchText != "") /*ignored when there are incoming updates from db, at rotation query takes place*/
        {
            menu.findItem(R.id.action_search).expandActionView()
            searchView.post { searchView.setQuery(searchText,false) }
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener
        {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                /*perform filtering*/
                Log.d("myItems", "filtering by $newText")
                searchText = newText /*searchView state management*/

                mAdapter.filter.filter(newText)
                return false
            }
        })
    }


}
