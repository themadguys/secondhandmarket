package com.themadguys.secondhandmarket.ui.showprofile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.themadguys.secondhandmarket.Rating
import com.themadguys.secondhandmarket.repository.UserRepo

class ShowProfileViewModel : ViewModel() {

    private val userRepo = UserRepo()
    var ratingsList = MutableLiveData<ArrayList<Rating>>()

    fun getRatings(userId : String) {
        val query = userRepo.getUserRatings(userId)
        query.addSnapshotListener { p0, _ ->
            val userList = ArrayList<Rating>()
            if(p0 != null) {
                //var count = 0
                //val mutex = Mutex()
                p0.documents.forEach {
                    it.toObject(Rating::class.java)?.let { it1 ->
                        /*userRepo.showUser(it1.buyerId)
                            .get()
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    val buyer = task.result!!.toObject(User::class.java)
                                    it1.buyerPhotoPath = buyer!!.photoPath
                                    userList.add(it1)
                                    ratingsList.value = userList //temp
                                    //mutex.lock()
                                    *//*count++
                                    if(count == p0.documents.size)
                                        ratingsList.value = userList*//*
                                }
                            }*/

                        userList.add(it1)
                    }
                }
                ratingsList.value = userList
            }
        }
    }

}
