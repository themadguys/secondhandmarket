package com.themadguys.secondhandmarket.ui.itemedit


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.themadguys.secondhandmarket.User
import com.themadguys.secondhandmarket.repository.ItemRepo

class ItemEditViewModel : ViewModel() {


    var itemexpirydate:String? = null
    var itemKey = ""
    var itemSubCat = ""
    var chosenPhotoPath = MutableLiveData<String>()
    /*for handling discard alert*/
    var initialPhotoPath : String? = null

    private val itemRepo = ItemRepo()
    var interestedUsersList = MutableLiveData<ArrayList<User>>()
    var selectedBuyer : User? = null
    var selectedPos = -1

    fun getMyItemInterestedUsers(adId : String) {
        val query = itemRepo.getMyItemInterestedUsers(adId)
        query.addSnapshotListener { p0, _ ->
            val userList = ArrayList<User>()
            if(p0 != null) {
                p0.documents.forEach {
                    it.toObject(User::class.java)?.let { it1 ->
                        userList.add(it1)
                    }
                }
                interestedUsersList.value = userList
            }
        }
    }
}
