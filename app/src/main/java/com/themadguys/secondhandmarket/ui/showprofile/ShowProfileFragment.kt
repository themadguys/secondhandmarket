package com.themadguys.secondhandmarket.ui.showprofile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.stfalcon.imageviewer.StfalconImageViewer
import com.themadguys.secondhandmarket.R
import com.themadguys.secondhandmarket.RatingsAdapter
import com.themadguys.secondhandmarket.SharedViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.show_profile_fragment.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ShowProfileFragment : Fragment() {

    companion object {
        fun newInstance() = ShowProfileFragment()
    }

    private lateinit var viewModel: SharedViewModel
    private lateinit var privateViewModel: ShowProfileViewModel
    private lateinit var mAdapter : RatingsAdapter

    @SuppressLint("RestrictedApi")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.show_profile_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity!!.let { viewModel = ViewModelProviders.of(it).get(SharedViewModel::class.java) }
        privateViewModel = ViewModelProviders.of(this).get(ShowProfileViewModel::class.java)

        mAdapter = RatingsAdapter(this)
        user_ratings_list.apply {
            setHasFixedSize(true)
            // use the layout manager set in the xml
            adapter = mAdapter
        }

        privateViewModel.ratingsList.observe(viewLifecycleOwner, Observer {
            /*interested users adapter UI*/
            if(it.isEmpty()){
                user_rating_view.text = getString(R.string.user_ratings_empty)
                user_ratings_list.visibility = View.GONE
            }
            else{
                user_rating_view.text = getString(R.string.user_ratings)
                user_ratings_list.visibility = View.VISIBLE
            }
            CoroutineScope(Default).launch {
                mAdapter.setDataset(it)

                /*calculating AVG rating*/
                var ratingAvg = 0.0F
                it.forEach { rating ->
                 ratingAvg += rating.score.toFloat()
                }
                ratingAvg /= it.size
                val ratingsQuantity = "(${it.size})"

                withContext(Main){
                    mAdapter.notifyDataSetChanged()
                    ratingBar.rating = ratingAvg
                    ratingsQty.text = ratingsQuantity
                }
            }
        })

        photo.setImageResource(R.drawable.ic_person_black_24dp)

        viewModel.publicUser.observe(viewLifecycleOwner, Observer {
            if (arguments != null){
                nickname.text = it.nickname
                email.text = it.email
                geographic_area.text = it.country
                if (it.photoPath != "")
                {
                    Glide.with(activity!!).load(it.photoPath).circleCrop().into(photo)
                    val images = ArrayList<String>()
                    images.add(it.photoPath) //useful in case of multiple images

                    photo.setOnTouchListener { v, _ ->
                        v.performClick()

                        StfalconImageViewer.Builder<String>(context, images) {
                                view, imageUrl ->
                            Glide.with(activity!!).load(imageUrl).into(view)
                        }.withStartPosition(0)
                            .withTransitionFrom(photo)
                            .show()

                        return@setOnTouchListener false
                    }
                }
                privateViewModel.getRatings(it.userId)
            }

        })

        if (arguments != null) {
            if (arguments?.containsKey("publicProfile")!!) {
                viewModel.showPublicUser(arguments!!.getString("publicProfile")!!)
                full_name.visibility = View.GONE
                textView2.visibility = View.GONE
                val toolbar: Toolbar = activity!!.findViewById(R.id.toolbar)
                toolbar.navigationIcon = ContextCompat.getDrawable(activity!!,R.drawable.ic_arrow_back_black_24dp)
            }
        } else {
            if (viewModel.user.value!!.photoPath != "")
                Glide.with(activity!!).load(viewModel.user.value!!.photoPath).circleCrop()
                    .into(photo)

            full_name.visibility = View.VISIBLE
            textView2.visibility = View.VISIBLE
            viewModel.userKey = viewModel.user.value!!.userId
            full_name.text = viewModel.user.value!!.name
            nickname.text = viewModel.user.value!!.nickname
            email.text = viewModel.user.value!!.email
            geographic_area.text = viewModel.user.value!!.country
            privateViewModel.getRatings(viewModel.user.value!!.userId)
        }

        geographic_area.setOnClickListener {
            Snackbar.make(view!!, "Viewing the location...", Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show()
            val bundle = bundleOf("locationAddress" to geographic_area.text.toString())
            bundle.putString("profileShowMode","showmode")
            findNavController().navigate(R.id.action_nav_showprofile_to_googleMapsFragment, bundle)
        }


    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (arguments == null)
            inflater.inflate(R.menu.edit_profile, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.editProfileFragment -> {
                if(arguments == null)
                    findNavController().navigate(R.id.action_nav_showprofile_to_editProfileFragment/*, bundle*/)
                true
            }
            else -> {
                if(arguments != null)
                {
                    activity!!.onBackPressed()
                    true
                }
                else
                    super.onOptionsItemSelected(item)
            }
        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (activity!!.drawer_layout.isDrawerOpen(GravityCompat.START))
                    activity!!.drawer_layout.closeDrawers()
                else
                    findNavController().navigateUp()
            }
        })
    }

}
