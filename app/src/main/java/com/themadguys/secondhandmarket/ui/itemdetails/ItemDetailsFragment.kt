package com.themadguys.secondhandmarket.ui.itemdetails

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.style.ImageSpan
import android.view.*
import android.widget.PopupMenu
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.stfalcon.imageviewer.StfalconImageViewer
import com.themadguys.secondhandmarket.InterestedUsersAdapter
import com.themadguys.secondhandmarket.R
import com.themadguys.secondhandmarket.SharedViewModel
import com.themadguys.secondhandmarket.User
import kotlinx.android.synthetic.main.item_details_fragment.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.collections.ArrayList


class ItemDetailsFragment : Fragment() {

    companion object {
        fun newInstance() = ItemDetailsFragment()
    }

    private lateinit var sharedViewModel: SharedViewModel
    private lateinit var viewModel: ItemDetailsViewModel
    private lateinit var mAdapter : InterestedUsersAdapter
    private var hintSnack : Snackbar? = null

    @SuppressLint("RestrictedApi")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.item_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity!!.let { sharedViewModel = ViewModelProviders.of(it).get(SharedViewModel::class.java) }
        viewModel = ViewModelProviders.of(this).get(ItemDetailsViewModel::class.java)

        mAdapter = InterestedUsersAdapter(this, 0)
        interested_users.apply {
            setHasFixedSize(true)
            // use the layout manager set in the xml
            adapter = mAdapter
        }
        mAdapter.setOnItemClickListener(object : InterestedUsersAdapter.OnItemClickListener {
            override fun onClick(user: User?, position: Int, itemView: View) {
                if(user != null)
                {
                    val bundle = bundleOf("publicProfile" to user.userId)
                    val popup = PopupMenu(activity,itemView)
                    popup.inflate(R.menu.sell_from_details)
                    popup.setOnMenuItemClickListener { item ->
                        when(item.itemId) {
                            R.id.sell_item -> {
                                AlertDialog.Builder(activity).setTitle("Sell item to ${user.nickname}")
                                    .setMessage("Are you sure?")
                                    .setPositiveButton("Yes") { dialog: DialogInterface, _: Int ->
                                        dialog.dismiss()
                                        val itemSold = viewModel.currentItem.value
                                        itemSold!!.itemStatus = "SOLD"
                                        sharedViewModel.setItem(itemSold,user,true)
                                        findNavController().navigate(R.id.action_nav_itemdetails_to_nav_itemlist)
                                    }
                                    .setNegativeButton("No") {
                                            dialog: DialogInterface, _: Int -> dialog.cancel()
                                    }
                                    .create().show()
                            }
                            R.id.show_profile -> {
                                findNavController().navigate(R.id.action_nav_itemdetails_to_nav_showprofile, bundle)
                            }
                        }
                        true
                    }
                    popup.show()
                }
            }
        })

        viewModel.isInterested.observe(viewLifecycleOwner, Observer { checked ->
            if (checked)
                fab_booking.setImageResource(R.drawable.ic_bookmark_unwatch)
            else
                fab_booking.setImageResource(R.drawable.ic_bookmark_watch)
        })

        viewModel.interestedUsersList.observe(viewLifecycleOwner, Observer {
            /*interested users adapter UI*/
            if(it.isEmpty()){
                users_view.text = getString(R.string.empty_user_list)
            }
            else{
                users_view.text = getString(R.string.interested_users)
            }
            CoroutineScope(Default).launch {
                mAdapter.setDataset(it)
                withContext(Main){
                    mAdapter.notifyDataSetChanged()
                }
            }
        })

        viewModel.sellerPhoto.observe(viewLifecycleOwner, Observer {
            sellerPhoto.setImageResource(R.drawable.ic_person_black_24dp)
            if(it != "")
                Glide.with(activity!!).load(it).circleCrop().into(sellerPhoto)
        })

        viewModel.ratingsList.observe(viewLifecycleOwner, Observer {

            CoroutineScope(Default).launch {

                /*calculating AVG rating*/
                var ratingAvg = 0.0F
                it.forEach { rating ->
                    ratingAvg += rating.score.toFloat()
                }
                ratingAvg /= it.size
                val ratingsQuantity = "(${it.size})"

                withContext(Main){
                    seller_rating_score.rating = ratingAvg
                    ratingsQty.text = ratingsQuantity
                }
            }
        })

        viewModel.currentItem.observe(viewLifecycleOwner, Observer { obj ->
            if(obj == null)
            {
                if(hintSnack != null)
                    hintSnack!!.dismiss()
                return@Observer
            }
            title.text = obj.title
            itemdescription.text = obj.description
            itemprice.text = obj.price
            itemcategory.text = obj.category
            itemlocation.text = obj.location
            itemexpirationdate.text = obj.expiryDate
            if (obj.photoPath != "")
            {
                Glide.with(activity!!).load(obj.photoPath).into(itemphoto)
                val images = ArrayList<String>()
                images.add(obj.photoPath) //useful in case of multiple images

                itemphoto.setOnTouchListener { v, _ ->
                    v.performClick()

                    StfalconImageViewer.Builder<String>(context, images) {
                            view, imageUrl ->
                        Glide.with(activity!!).load(imageUrl).into(view)
                    }.withStartPosition(0)
                        .withTransitionFrom(itemphoto)
                        .show()

                    return@setOnTouchListener false
                }
            }
            else
                itemphoto.setImageResource(R.drawable.ic_insert_photo_black_24dp)


            if (obj.ownerId == sharedViewModel.userKey) {
                /*my details*/
                fab_booking.hide()
                users_view.visibility = View.VISIBLE
                interested_users.visibility = View.VISIBLE
                dividerStartSeller.visibility = View.GONE
                sellerLayout.visibility = View.GONE
                dividerEndSeller.visibility = View.GONE

                if(obj.itemStatus == "EXPIRED")
                {
                    Snackbar.make(requireView(), "This advertisement is expired, please update it!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show()
                }
                setMenuVisibility(true)
                viewModel.getMyItemInterestedUsers(obj.adId)

                itemlocation.setOnClickListener {
                    Snackbar.make(view!!, "Viewing the location...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show()
                    val bundle = bundleOf("locationAddress" to itemlocation.text.toString())
                    bundle.putString("profileShowMode","showmode")
                    findNavController().navigate(R.id.action_nav_itemdetails_to_googleMapsFragment, bundle)
                }
            } else {
                /*onSale details*/
                fab_booking.show()
                users_view.visibility = View.GONE
                interested_users.visibility = View.GONE
                dividerStartSeller.visibility = View.VISIBLE
                sellerLayout.visibility = View.VISIBLE
                dividerEndSeller.visibility = View.VISIBLE
                setMenuVisibility(false)
                viewModel.isInterested(obj.adId, obj.ownerId)
                nickname.text = obj.ownerNickname
                viewModel.getSellerPhoto(obj.ownerId)
                viewModel.getRatings(obj.ownerId)
                if (!sharedViewModel.hintInterest)
                {
                    val hint = SpannableStringBuilder()
                    hint.append("You can add or remove your interest to an item with  ")
                    hint.setSpan(ImageSpan(activity!!,R.drawable.hint_interest),hint.length-1,hint.length,0)
                    hint.append(" button")
                    hintSnack = Snackbar.make(requireView(), hint, Snackbar.LENGTH_INDEFINITE)
                        .setActionTextColor(ContextCompat.getColor(activity!!, R.color.colorAccent))
                    hintSnack!!.setAction("OK") {
                        sharedViewModel.hintInterest = true
                        hintSnack!!.dismiss()
                    }
                    hintSnack!!.show()
                }

                val bundle = bundleOf("publicProfile" to obj.ownerId)

                if(obj.itemStatus != "ONSALE")
                {
                    if(hintSnack != null)
                        hintSnack!!.dismiss()
                    AlertDialog.Builder(activity).setTitle("Item status notification")
                        .setMessage("This item is no more on sale!")
                        .setPositiveButton("Ok, go back.") { dialog: DialogInterface, _: Int ->
                            dialog.dismiss()
                            if(arguments?.getString("navController").toString() == "ItemsOfInterestListFragment")
                                findNavController().navigate(R.id.action_nav_itemdetails_to_nav_itemsOfInterestListFragment)
                            else if(arguments?.getString("navController").toString() == "OnSaleListFragment")
                                findNavController().navigate(R.id.action_nav_itemdetails_to_nav_onSaleListFragment)
                        }.setIcon(R.drawable.ic_warning_black_24dp).setCancelable(false).create().show()
                }

                sellerLayout.setOnClickListener {
                    if(hintSnack != null)
                        hintSnack!!.dismiss()
                    findNavController().navigate(R.id.action_nav_itemdetails_to_nav_showprofile, bundle)
                }

                itemlocation.setOnClickListener {
                    if(hintSnack != null)
                        hintSnack!!.dismiss()
                    Snackbar.make(view!!, "Getting directions to the location...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show()
                    val bundleDirections = bundleOf("locationAddress" to itemlocation.text.toString())
                    bundleDirections.putString("itemShowMode","showmode")
                    findNavController().navigate(R.id.action_nav_itemdetails_to_googleMapsFragment, bundleDirections)
                }
            }
        })
        var owner = sharedViewModel.userKey
        if (arguments?.containsKey("itemOwner")!!)
            owner = arguments?.getString("itemOwner").toString()
        viewModel.getItem(arguments?.getString("itemAdKey").toString(), owner)

        fab_booking.setOnClickListener {
            if (viewModel.isInterested.value!!) {
                viewModel.removeInterestedUser(
                    viewModel.currentItem.value!!.adId,
                    viewModel.currentItem.value!!.ownerId
                )
                Snackbar.make(it, "Interest removed...", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            } else {
                viewModel.addInterestedUser(
                    viewModel.currentItem.value!!,
                    sharedViewModel.user.value!!
                )
                Snackbar.make(it, "You are now interested to ${viewModel.currentItem.value!!.title}", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.edit_button, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.itemEditFragment -> {
                val bundle = bundleOf("itemAdKey" to viewModel.currentItem.value!!.adId)
                findNavController().navigate(
                    R.id.action_nav_itemdetails_to_itemEditFragment,
                    bundle
                )
                true
            }
            else -> {
                handleBackNav()
                true
            }
        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                handleBackNav()
            }
        })
    }

    private fun handleBackNav() {

        if(hintSnack != null)
            hintSnack!!.dismiss()

        if (sharedViewModel.userKey == viewModel.currentItem.value!!.ownerId)
            findNavController().navigate(R.id.action_nav_itemdetails_to_nav_itemlist)
        else
        {
            if(arguments?.getString("navController").toString() == "ItemsOfInterestListFragment")
                findNavController().navigate(R.id.action_nav_itemdetails_to_nav_itemsOfInterestListFragment)
            else if(arguments?.getString("navController").toString() == "OnSaleListFragment")
                findNavController().navigate(R.id.action_nav_itemdetails_to_nav_onSaleListFragment)
        }
    }


}
