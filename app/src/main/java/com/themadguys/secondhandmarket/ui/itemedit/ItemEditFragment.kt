package com.themadguys.secondhandmarket.ui.itemedit

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ScrollView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.themadguys.secondhandmarket.Item
import com.themadguys.secondhandmarket.R
import com.themadguys.secondhandmarket.*
import kotlinx.android.synthetic.main.item_edit_fragment.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import java.io.File
import java.util.*

class ItemEditFragment : Fragment(), AdapterView.OnItemSelectedListener {

    companion object {
        fun newInstance() = ItemEditFragment()
    }

    private lateinit var viewModel: SharedViewModel
    private lateinit var privateViewModel: ItemEditViewModel
    private lateinit var thisItem : Item
    private var subCatFromView = ""
    private lateinit var mAdapter : InterestedUsersAdapter
    private var shortAnimationDuration = 0

    @SuppressLint("RestrictedApi")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.item_edit_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
        registerForContextMenu(camera)
        camera.setOnClickListener{
            Toast.makeText(activity, "Long Press to show options", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity!!.let { viewModel = ViewModelProviders.of(it).get(SharedViewModel::class.java) }
        privateViewModel = ViewModelProviders.of(this).get(ItemEditViewModel::class.java)

        mAdapter = InterestedUsersAdapter(this, privateViewModel.selectedPos)
        interested_users.apply {
            setHasFixedSize(true)
            // use the layout manager set in the xml
            adapter = mAdapter
        }
        mAdapter.setOnItemClickListener(object : InterestedUsersAdapter.OnItemClickListener {
            override fun onClick(
                user: User?,
                position: Int,
                itemView: View
            ) {
                if(user != null)
                {
                    privateViewModel.selectedBuyer = user
                }
                else
                {
                    privateViewModel.selectedBuyer = null
                }
                privateViewModel.selectedPos = position
            }
        })

        privateViewModel.chosenPhotoPath.observe(viewLifecycleOwner, Observer {
            itemphoto.setImageResource(R.drawable.ic_insert_photo_black_24dp)
            Glide.with(activity!!).asBitmap().load(privateViewModel.chosenPhotoPath.value).into(itemphoto)
        })
        /*populating spinner*/
        val adapter = ArrayAdapter.createFromResource(
            activity!!,
            R.array.itemcat_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            itemcategory.adapter = adapter
        }
        itemcategory.onItemSelectedListener = this

        privateViewModel.interestedUsersList.observe(viewLifecycleOwner, Observer {
            /*interested users adapter UI*/
            if(it.isEmpty()){
                users_view.text = getString(R.string.empty_user_list)
            }
            else{
                users_view.text = getString(R.string.interested_users)
            }
            CoroutineScope(Default).launch {
                mAdapter.setDataset(it)
                withContext(Main){
                    mAdapter.notifyDataSetChanged()
                }
            }
        })

        status_choices.setOnCheckedChangeListener { _, checkedId ->
                when(checkedId){
                    R.id.blockAd_choice -> {
                        shortAnimationDuration = resources.getInteger(android.R.integer.config_mediumAnimTime)
                        users_view.animate()
                            .alpha(0f)
                            .setDuration(shortAnimationDuration.toLong())
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    users_view.visibility = View.GONE
                                }
                            })
                        interested_users.animate()
                            .alpha(0f)
                            .setDuration(shortAnimationDuration.toLong())
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    interested_users.visibility = View.GONE
                                }
                            })

                    }
                    R.id.sellItem_choice -> {
                        if(privateViewModel.selectedBuyer == null)
                        {
                            Snackbar.make(view!!, "Select a user whom you want to sell this item to", Snackbar.LENGTH_LONG)
                                .setAction("Action", null)
                                .show()
                        }

                        shortAnimationDuration = resources.getInteger(android.R.integer.config_mediumAnimTime)
                        users_view.apply {
                            alpha = 0f
                            visibility = View.VISIBLE
                        }
                        interested_users.apply {
                            alpha = 0f
                            visibility = View.VISIBLE
                        }
                        scrollEditor.post {
                            scrollEditor.fullScroll(ScrollView.FOCUS_DOWN)
                            users_view.animate().alpha(1f).setDuration(shortAnimationDuration.toLong()).setListener(null)
                            interested_users.animate().alpha(1f).setDuration(shortAnimationDuration.toLong()).setListener(null)
                        }
                    }
                    R.id.onSale_choice -> {
                        shortAnimationDuration = resources.getInteger(android.R.integer.config_mediumAnimTime)
                        users_view.animate()
                            .alpha(0f)
                            .setDuration(shortAnimationDuration.toLong())
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    users_view.visibility = View.GONE
                                }
                            })
                        interested_users.animate()
                            .alpha(0f)
                            .setDuration(shortAnimationDuration.toLong())
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    interested_users.visibility = View.GONE
                                }
                            })
                    }
                }
        }

        /*populating with cardId taken from navigation*/
        arguments?.let {

            if(arguments!!.containsKey("itemAdKey"))
            {
                privateViewModel.itemKey = arguments?.getString("itemAdKey")!!
                if(privateViewModel.itemKey != "")
                {
                    thisItem = viewModel.getMyItem(privateViewModel.itemKey)
                    //setItemToView(thisItem, adapter)
                }
            }
            if(arguments!!.containsKey("locationAddress"))
            {
                privateViewModel.itemKey = arguments?.getString("itemAdKey")!!
                val jSONdata = arguments!!.getString("itemFromView")!!
                val itemFromView = Json(JsonConfiguration.Stable)
                    .parse(Item.serializer(),jSONdata)
                setItemToView(itemFromView, adapter)
                if(arguments!!.getString("locationAddress") != "")
                    itemlocation.setText(arguments!!.getString("locationAddress")!!)
            }
            else
                setItemToView(thisItem, adapter)
        }

        if(privateViewModel.itemKey == "") /*if new AD, remove SELL choice from radio buttons*/
            sellItem_choice.visibility = View.GONE

        itemlocation.setOnTouchListener { v, event ->
            v.performClick()
            if(event.action == MotionEvent.ACTION_UP) {
                if(event.x >= (itemlocation.width - itemlocation.compoundDrawables[2].bounds.width())) {

                    val jSONdata = Json(JsonConfiguration.Stable)
                        .stringify(Item.serializer(),getItemFromView(getItemStatusFromRadioButtons()))
                    val bundle = bundleOf("itemFromView" to jSONdata)
                    bundle.putString("itemAdKey",privateViewModel.itemKey)
                    bundle.putString("locationAddress",itemlocation.text.toString())
                    if(activity!!.currentFocus != null)
                    {
                        val keyboardOut = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        keyboardOut.hideSoftInputFromWindow(activity!!.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                    }
                    findNavController().navigate(R.id.action_itemEditFragment_to_googleMapsFragment, bundle)
                    return@setOnTouchListener true
                }
            }
            return@setOnTouchListener false
        }


        if(privateViewModel.itemexpirydate != null)
        {
            val expDate = privateViewModel.itemexpirydate!!.split("/")
            itemexpirationdate.updateDate(expDate[2].toInt(),expDate[1].toInt()-1,expDate[0].toInt())
        }



    }

    override fun onDestroyView() {
        val monthYear = itemexpirationdate.month + 1
        privateViewModel.itemexpirydate = itemexpirationdate.dayOfMonth.toString() + "/" + monthYear.toString() + "/" + itemexpirationdate.year.toString()
        privateViewModel.itemSubCat = itemsubcategory.selectedItem.toString()
        super.onDestroyView()
    }


    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?){
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = activity!!.menuInflater
        inflater.inflate(R.menu.picture_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.select_from_gallery -> {
                if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M)
                {
                    if(ContextCompat.checkSelfPermission(activity!!,android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
                    {
                        //denied
                        val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                        //popup activity
                        requestPermissions(permissions,REQUEST_PERMISSION_GALLERY)
                    }
                    else
                    {
                        //already granted
                        pickPhotoFromGallery(this)
                    }
                }
                else {
                    //OS < M
                    pickPhotoFromGallery(this)
                }
                true
            }
            R.id.take_photo -> {
                if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M)
                {
                    if(ContextCompat.checkSelfPermission(activity!!,android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED)
                    {
                        //denied
                        val permissions = arrayOf(android.Manifest.permission.CAMERA)
                        //popup activity
                        requestPermissions(permissions,REQUEST_PERMISSION_CAMERA)
                    }
                    else
                    {
                        //already granted
                        takePhotoFromCamera("itemPic_" + privateViewModel.itemKey, this)
                    }
                }
                else {
                    //OS < M
                    takePhotoFromCamera("itemPic_" + privateViewModel.itemKey, this)
                }
                true
            }
            else -> {
                super.onContextItemSelected(item)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode)
        {
            REQUEST_PERMISSION_GALLERY -> {
                if(grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    //user is agree
                    pickPhotoFromGallery(this)
                }
                else
                {
                    //user is not agree
                    Toast.makeText(activity, "Permission DENIED!", Toast.LENGTH_SHORT).show()
                }
            }
            REQUEST_PERMISSION_CAMERA -> {
                if(grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    //user is agree
                    takePhotoFromCamera("itemPic_" + privateViewModel.itemKey, this)
                }
                else
                {
                    //user is not agree
                    Toast.makeText(activity, "Permission DENIED!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_PHOTO && resultCode == Activity.RESULT_OK) {
            Toast.makeText(activity, "Picture SAVED!", Toast.LENGTH_SHORT).show()
            privateViewModel.chosenPhotoPath.value = Uri.fromFile(File(currentPhotoPath)).toString()
        }
        else if(requestCode == REQUEST_PHOTO_FROM_GALLERY && resultCode == Activity.RESULT_OK)
        {
            val URItmp = data?.data as Uri
            Toast.makeText(activity, "Picture LOADED!", Toast.LENGTH_SHORT).show()
            val photoFile: File? = createImageFile("itemPic_" + privateViewModel.itemKey, this)
            val fileOut = context!!.contentResolver.openOutputStream(Uri.fromFile(photoFile))
            val fileIn = context!!.contentResolver.openInputStream(URItmp)
            fileIn!!.copyTo(fileOut!!)
            privateViewModel.chosenPhotoPath.value = Uri.fromFile(File(currentPhotoPath)).toString()
        }
    }



    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.save_button, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(activity!!.currentFocus != null)
        {
            val keyboardOut = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboardOut.hideSoftInputFromWindow(activity!!.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
        return when(item.itemId)
        {
            R.id.nav_itemlist -> { /*user clicks Save*/

                val expiration = Calendar.getInstance()
                val currentDate = Calendar.getInstance()
                expiration.set(itemexpirationdate.year, itemexpirationdate.month, itemexpirationdate.dayOfMonth)

                var notifyUsers = false
                var oldStatus = "ONSALE"
                if(::thisItem.isInitialized)
                    oldStatus = thisItem.itemStatus

                val itemstatus = getItemStatusFromRadioButtons()

                if(oldStatus != itemstatus)
                    notifyUsers = true

                /*check fields emptiness*/
                if(TextUtils.isEmpty(title.text.trim().toString()) || TextUtils.isEmpty(itemdescription.text.trim().toString()) || TextUtils.isEmpty(itemprice.text.toString()) || TextUtils.isEmpty(itemlocation.text.trim().toString()))
                {
                    Toast.makeText(activity, "Empty field not allowed", Toast.LENGTH_SHORT).show()
                }
                else if(expiration < currentDate)
                {
                    Toast.makeText(activity, "You cannot sell an item in the past!", Toast.LENGTH_SHORT).show()
                }
                else if(itemstatus == "SOLD" && privateViewModel.selectedBuyer == null)
                {
                    Toast.makeText(activity, "You must select a buyer!", Toast.LENGTH_SHORT).show()
                }
                else
                {
                    AlertDialog.Builder(activity).setTitle("Save this advertisement").setMessage("Are you sure?").setPositiveButton("Yes"){
                            dialog: DialogInterface, _: Int -> dialog.dismiss()

                        thisItem = getItemFromView(itemstatus)
                        if(itemstatus != "SOLD")
                            privateViewModel.selectedBuyer = null

                        Toast.makeText(activity, "Info saved! Loading...", Toast.LENGTH_LONG).show()
                        viewModel.setItem(thisItem, privateViewModel.selectedBuyer, notifyUsers)

                        NavigationUI.onNavDestinationSelected(item, view!!.findNavController())
                    }.setNegativeButton("No"){
                            dialog: DialogInterface, _: Int -> dialog.cancel()
                    }.create().show()

                }

                true
            }
            else -> {
                handleBackAlert()
                true
            }
        }

    }

    private fun getItemFromView(itemstatus : String): Item {
        val itemtitle = title.text.trim().toString()
        val itemdescription = itemdescription.text.trim().toString()
        val itemprice = itemprice.text.toString()
        val itemcategory = itemcategory.selectedItem.toString() + ": " + itemsubcategory.selectedItem.toString()
        val itemlocation = itemlocation.text.trim().toString()

        val itemexpirydate = parseDate()

        return if(privateViewModel.chosenPhotoPath.value != null) {
            Item(privateViewModel.itemKey,viewModel.user.value!!.userId,privateViewModel.chosenPhotoPath.value!!,itemtitle,itemdescription,itemprice,itemcategory,itemlocation,itemexpirydate,itemstatus,viewModel.user.value!!.nickname)
        } else
            Item(privateViewModel.itemKey,viewModel.user.value!!.userId,"",itemtitle,itemdescription,itemprice,itemcategory,itemlocation,itemexpirydate,itemstatus,viewModel.user.value!!.nickname)

    }

    private fun setItemToView(item: Item, adapter: ArrayAdapter<CharSequence>) {

        if(item.photoPath != "")
            privateViewModel.chosenPhotoPath.value = item.photoPath
        privateViewModel.initialPhotoPath = privateViewModel.chosenPhotoPath.value
        title.setText(item.title)
        itemdescription.setText(item.description)
        itemprice.setText(item.price)
        val itemCat = item.category.split(": ")[0]
        subCatFromView = item.category.split(": ")[1]

        itemcategory.setSelection(adapter.getPosition(itemCat))

        itemlocation.setText(item.location)
        val expDate = item.expiryDate.split("/")

        itemexpirationdate.updateDate(expDate[0].toInt(),expDate[1].toInt()-1,expDate[2].toInt())

        if(item.itemStatus == "UNAVAILABLE")
            status_choices.check(R.id.blockAd_choice)
        else if(item.itemStatus == "SOLD")
            status_choices.check(R.id.sellItem_choice)
        else if(item.itemStatus == "EXPIRED")
            Snackbar.make(requireView(), "This advertisement is expired, please update it!", Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show()
        else if(item.itemStatus == "ONSALE")
            status_choices.check(R.id.onSale_choice)

        if(item.adId != "")
            privateViewModel.getMyItemInterestedUsers(item.adId)
    }

    private fun getItemStatusFromRadioButtons() : String {

        val choiceId = status_choices.checkedRadioButtonId
        var itemstatus = "ONSALE"
        if(choiceId == R.id.blockAd_choice)
            itemstatus = "UNAVAILABLE"
        else if(choiceId == R.id.sellItem_choice)
            itemstatus = "SOLD"

        return itemstatus
    }


    private fun parseDate(): String {
        val monthYear = itemexpirationdate.month + 1
        val dayMonth = itemexpirationdate.dayOfMonth
        var month = "$monthYear"
        var day = "$dayMonth"
        if(monthYear < 10)
            month = "0$monthYear"
        if(dayMonth < 10)
            day = "0$dayMonth"
        return itemexpirationdate.year.toString() + "/" + month + "/" + day
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                handleBackAlert()
            }
        })
    }

    private fun handleBackAlert(){
        if(privateViewModel.itemKey == "") /*if is the FAB one*/
        {
            AlertDialog.Builder(activity).setTitle("Discard this new advertisement").setMessage("Are you sure?").setPositiveButton("Yes"){
                    dialog: DialogInterface, _: Int -> dialog.dismiss()
                findNavController().navigate(R.id.action_itemEditFragment_to_nav_itemlist)
            }.setNegativeButton("No"){
                    dialog: DialogInterface, _: Int -> dialog.cancel()
            }.create().show()
        }
        else
        {
            /*before navigation check modified data*/
            if( privateViewModel.initialPhotoPath == privateViewModel.chosenPhotoPath.value &&
                thisItem.title == title.text.trim().toString() &&
                thisItem.description == itemdescription.text.trim().toString() &&
                thisItem.price == itemprice.text.toString() &&
                thisItem.location == itemlocation.text.trim().toString() &&
                thisItem.category == itemcategory.selectedItem.toString() + ": " + itemsubcategory.selectedItem.toString() &&
                thisItem.expiryDate == parseDate())
            {
                if(arguments?.getString("nav") == "editCard")
                    findNavController().navigate(R.id.action_itemEditFragment_to_nav_itemlist)
                else
                {
                    val bundle = bundleOf("itemAdKey" to privateViewModel.itemKey)
                    findNavController().navigate(R.id.action_itemEditFragment_to_nav_itemdetails, bundle)
                }
            }
            else
                AlertDialog.Builder(activity).setTitle("Discard edit").setMessage("Are you sure?").setPositiveButton("Yes"){
                        dialog: DialogInterface, _: Int -> dialog.dismiss()
                    if(arguments?.getString("nav") == "editCard")
                        findNavController().navigate(R.id.action_itemEditFragment_to_nav_itemlist)
                    else
                    {
                        val bundle = bundleOf("itemAdKey" to privateViewModel.itemKey)
                        findNavController().navigate(R.id.action_itemEditFragment_to_nav_itemdetails, bundle)
                    }
                }.setNegativeButton("No"){
                        dialog: DialogInterface, _: Int -> dialog.cancel()
                }.create().show()
        }
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        Log.d("kkk","HO CLICCATO da: $parent")
        if(parent == itemcategory)
        {
            var subcat = R.array.itemcat_array
            when(position){
                0 -> subcat = R.array.itemsubcat1_array
                1 -> subcat = R.array.itemsubcat2_array
                2 -> subcat = R.array.itemsubcat3_array
                3 -> subcat = R.array.itemsubcat4_array
                4 -> subcat = R.array.itemsubcat5_array
                5 -> subcat = R.array.itemsubcat6_array
                6 -> subcat = R.array.itemsubcat7_array
                7 -> subcat = R.array.itemsubcat8_array
            }

            val adapter = ArrayAdapter.createFromResource(
                activity!!,
                subcat,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                itemsubcategory.adapter = adapter
            }
            if(::thisItem.isInitialized) /*checks if is in Editing mode*/
            {
                val subCat = thisItem.category.split(": ")[1]
                itemsubcategory.setSelection(adapter.getPosition(subCat))
            }
            if(subCatFromView != "") /*checks if return from Maps fragment*/
            {
                itemsubcategory.setSelection(adapter.getPosition(subCatFromView))
            }
            if(privateViewModel.itemSubCat != "") /*checks device rotations*/
                itemsubcategory.setSelection(adapter.getPosition(privateViewModel.itemSubCat))
        }




    }


}