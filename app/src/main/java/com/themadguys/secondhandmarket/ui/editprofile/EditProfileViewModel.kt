package com.themadguys.secondhandmarket.ui.editprofile


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class EditProfileViewModel : ViewModel() {

    var chosenPhotoPath = MutableLiveData<String>()
    var initialPhotoPath : String? = null /*for handling discard alert*/

}
