package com.themadguys.secondhandmarket.ui.googlemaps

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar
import com.themadguys.secondhandmarket.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
import com.themadguys.secondhandmarket.R
import com.themadguys.secondhandmarket.SharedViewModel
import kotlinx.android.synthetic.main.google_maps_fragment.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*


class GoogleMapsFragment : Fragment() {

    companion object {
        fun newInstance() = GoogleMapsFragment()
    }

    private lateinit var viewModel: GoogleMapsViewModel
    private lateinit var sharedViewModel: SharedViewModel
    private lateinit var gMaps : GoogleMap
    private var searchText = ""
    private var myPositionDefault = LatLng(45.064275, 7.659443) //DAUIN

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.google_maps_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        mapView.onCreate(savedInstanceState)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(GoogleMapsViewModel::class.java)
        sharedViewModel = activity!!.run { ViewModelProviders.of(this).get(SharedViewModel::class.java) }

        if(viewModel.fusedLocationProviderClient == null)
            viewModel.fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity!!)

        if(viewModel.address == "")
            viewModel.address = arguments!!.getString("locationAddress")!!

        mapView.getMapAsync{
            gMaps = it

            setMapLocation(viewModel.address)

            gMaps.setOnMapClickListener {
                if(arguments!!.containsKey("profileShowMode")  || arguments!!.containsKey("itemShowMode"))
                    Snackbar.make(view!!, "Long Press disabled in show mode", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null)
                        .show()
                else
                    Snackbar.make(view!!, "Long Press to add a marker", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null)
                        .show()
            }
            gMaps.setOnMapLongClickListener { point ->

                if(arguments!!.containsKey("profileShowMode") || arguments!!.containsKey("itemShowMode"))
                    Snackbar.make(view!!, "Long Press disabled in show mode", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null)
                        .show()
                else
                {
                    Geocoder(activity, Locale.getDefault())
                        .getFromLocation(point.latitude,point.longitude,1)
                        .apply {
                            viewModel.address = get(0).getAddressLine(0)
                        }

                    gMaps.clear()
                    gMaps.animateCamera(CameraUpdateFactory.newLatLngZoom(point,15.0F))
                    gMaps.addMarker(MarkerOptions().position(point).title(viewModel.address))
                }
            }
        }

        if(savedInstanceState != null && savedInstanceState.containsKey("searchText"))
            searchText = savedInstanceState.getString("searchText")!!

    }

    private fun setMapLocation(address: String) {

        if(sharedViewModel.isNetworkConnected.value == false)
        {
            Snackbar.make(view!!, "Check your internet connection!", Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show()
            return
        }
        if(address == "")
        {
            Snackbar.make(view!!, "Select a location...", Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show()
            return
        }
        getLocationPermission()
        //updateLocationUI()

        setMarkersUI(address)


    }

    private fun setMarkersUI(address: String) {

        CoroutineScope(IO).launch {
            var locationAddress : LatLng
            var addresses = Geocoder(activity, Locale.getDefault())
                .getFromLocationName(address,1)

            if(addresses.size == 0)
                addresses = Geocoder(activity, Locale.ITALY)
                    .getFromLocationName(address,1)

            withContext(Main) {
                if(addresses.size > 0)
                {
                    viewModel.address = address
                    locationAddress = LatLng(addresses[0].latitude, addresses[0].longitude)
                    gMaps.clear()
                    gMaps.moveCamera(CameraUpdateFactory.newLatLngZoom(locationAddress,15.0F))
                    gMaps.addMarker(MarkerOptions().position(locationAddress).title(address)
                        .icon(BitmapDescriptorFactory.defaultMarker(42.0F)))
                    if(arguments!!.containsKey("itemShowMode"))
                        getDeviceLocation(locationAddress)
                }
                else
                    Snackbar.make(view!!, "Can't find location typed!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show()
            }
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if(searchText != "")
            outState.putString("searchText", searchText)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if(!arguments!!.containsKey("profileShowMode") && !arguments!!.containsKey("itemShowMode"))
        {
            inflater.inflate(R.menu.search_filter, menu)
            inflater.inflate(R.menu.save_profile, menu)
            val searchView = menu.findItem(R.id.action_search).actionView as SearchView
            searchView.maxWidth = Integer.MAX_VALUE
            searchView.queryHint = "Search by location name, place, etc.."
            if(searchText != "") /*ignored when there are incoming updates from db, at rotation query takes place*/
            {
                menu.findItem(R.id.action_search).expandActionView()
                searchView.post { searchView.setQuery(searchText,false) }
            }
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener
            {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    query?.let {
                        setMapLocation(query)
                        searchText = ""
                        menu.findItem(R.id.action_search).collapseActionView()
                    }
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    /*perform filtering*/
                    Log.d("myItems", "filtering by $newText")
                    searchText = newText /*searchView state management*/

                    return false
                }
            })
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId)
        {
            R.id.nav_showprofile -> {

                if(activity!!.currentFocus != null)
                {
                    val keyboardOut = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    keyboardOut.hideSoftInputFromWindow(activity!!.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                }

                handleBackPress(viewModel.address)
                true
            }
            R.id.action_search -> {
                true
            }
            else -> {
                handleBackPress("")
                true
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                handleBackPress("")
            }
        })
    }

    private fun handleBackPress(usedLocation : String) {
        val bundle = bundleOf("locationAddress" to usedLocation)
        if(arguments!!.containsKey("userFullName"))
        {
            bundle.putString("locationAddress",viewModel.address)
            bundle.putString("userFullName",arguments!!.getString("userFullName")!!)
            bundle.putString("userNickname",arguments!!.getString("userNickname")!!)
            bundle.putString("userEmail",arguments!!.getString("userEmail")!!)
            bundle.putString("userPhoto",arguments!!.getString("userPhoto")!!)
            findNavController().navigate(R.id.action_googleMapsFragment_to_editProfileFragment, bundle)
        }
        else if(arguments!!.containsKey("itemAdKey"))
        {
            bundle.putString("itemAdKey",arguments!!.getString("itemAdKey")!!)
            bundle.putString("itemFromView",arguments!!.getString("itemFromView")!!)
            findNavController().navigate(R.id.action_googleMapsFragment_to_itemEditFragment, bundle)
        }
        else
        {
            findNavController().navigateUp()
        }
    }


    override fun onResume() {
        mapView.onResume()
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroyView() {
        mapView.onDestroy()
        super.onDestroyView()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    private fun getLocationPermission() {

        if (ContextCompat.checkSelfPermission(activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            viewModel.locationPermissionGranted = true
            updateLocationUI()
        } else {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        viewModel.locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    viewModel.locationPermissionGranted = true
                    setMarkersUI(viewModel.address)
                }
            }
        }
        updateLocationUI()
    }

    private fun updateLocationUI() {
        try {
            if (viewModel.locationPermissionGranted) {
                gMaps.isMyLocationEnabled = true
                gMaps.uiSettings.isMyLocationButtonEnabled = true
            } else {
                gMaps.isMyLocationEnabled = false
                gMaps.uiSettings.isMyLocationButtonEnabled = false
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    private fun getDeviceLocation(locationAddress: LatLng) {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        var myAddress = Geocoder(activity, Locale.getDefault())
            .getFromLocationName(sharedViewModel.user.value!!.country,1)
        if(myAddress.size <= 0)
            myAddress = Geocoder(activity, Locale.ITALY)
                .getFromLocationName(sharedViewModel.user.value!!.country,1)
        if(myAddress.size > 0)
            myPositionDefault = LatLng(myAddress[0].latitude, myAddress[0].longitude)

        viewModel.myPosition = LatLng(myPositionDefault.latitude,
            myPositionDefault.longitude)
        try {
            if (viewModel.locationPermissionGranted) {

                val locationResult = viewModel.fusedLocationProviderClient!!.lastLocation
                locationResult.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        viewModel.lastKnownLocation = task.result
                        if (viewModel.lastKnownLocation != null) {
                            viewModel.myPosition = LatLng(viewModel.lastKnownLocation!!.latitude,
                                viewModel.lastKnownLocation!!.longitude)
                        }
                        else
                        {
                            Snackbar.make(view!!, "Can't find GPS location! Using default...", Snackbar.LENGTH_LONG)
                                .setAction("Action", null)
                                .show()
                            gMaps.uiSettings?.isMyLocationButtonEnabled = false
                        }
                    }
                    else {
                        Snackbar.make(view!!, "Can't find GPS location! Using default...", Snackbar.LENGTH_LONG)
                            .setAction("Action", null)
                            .show()
                        Log.e("gMaps", "Exception: %s", task.exception)
                        viewModel.myPosition = LatLng(myPositionDefault.latitude,
                            myPositionDefault.longitude)
                        gMaps.uiSettings?.isMyLocationButtonEnabled = false
                    }
                    gMaps.addMarker(MarkerOptions().position(viewModel.myPosition!!).title("My position")
                        .icon(BitmapDescriptorFactory.defaultMarker(204.0F)))
                    gMaps.addPolyline(PolylineOptions().clickable(false).add(locationAddress).add(viewModel.myPosition!!))
                    gMaps.animateCamera(CameraUpdateFactory.newLatLngBounds(LatLngBounds.Builder()
                        .include(locationAddress)
                        .include(viewModel.myPosition!!)
                        .build(),50))
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }



}
