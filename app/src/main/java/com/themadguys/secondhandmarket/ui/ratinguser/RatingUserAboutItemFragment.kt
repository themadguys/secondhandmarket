package com.themadguys.secondhandmarket.ui.ratinguser

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.themadguys.secondhandmarket.R
import com.themadguys.secondhandmarket.Rating
import com.themadguys.secondhandmarket.SharedViewModel
import kotlinx.android.synthetic.main.rating_user_about_item_fragment.*

class RatingUserAboutItemFragment : Fragment() {

    companion object {
        fun newInstance() = RatingUserAboutItemFragment()
    }

    private lateinit var viewModel: RatingUserAboutItemViewModel
    private lateinit var sharedViewModel: SharedViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.rating_user_about_item_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(RatingUserAboutItemViewModel::class.java)
        activity!!.let { sharedViewModel = ViewModelProviders.of(it).get(SharedViewModel::class.java) }

        viewModel.sellerPhoto.observe(viewLifecycleOwner, Observer {
            photoSeller.setImageResource(R.drawable.ic_person_black_24dp)
            if(it != "")
                Glide.with(activity!!).load(it).circleCrop().into(photoSeller)
        })

        viewModel.sellerId = arguments?.getString("itemOwner")!!
        viewModel.itemPhoto = arguments?.getString("itemPhoto")!!
        viewModel.itemId = arguments?.getString("itemAdKey")!!
        itemphoto.setImageResource(R.drawable.ic_insert_photo_black_24dp)
        if(viewModel.itemPhoto != "")
            Glide.with(activity!!).load(viewModel.itemPhoto).into(itemphoto)
        viewModel.getSellerPhoto(viewModel.sellerId)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.save_button, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(activity!!.currentFocus != null)
        {
            val keyboardOut = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboardOut.hideSoftInputFromWindow(activity!!.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
        return when(item.itemId)
        {
            R.id.nav_itemlist -> { /*user clicks Save*/

                var ratingTitle = rating_title.text.toString()
                var ratingComment = rating_comment.text.toString()

                if(TextUtils.isEmpty(rating_title.text.trim().toString()))
                {
                    ratingTitle = "No title"
                }
                if(TextUtils.isEmpty(rating_comment.text.trim().toString()))
                {
                    ratingComment = ""
                }

                AlertDialog.Builder(activity).setTitle("Save this rating").setMessage("Are you sure?").setPositiveButton("Yes"){
                        dialog: DialogInterface, _: Int -> dialog.dismiss()

                    /* save all rating info to /users/{sellerId}/myratings/{newId} */
                    val myRating = Rating(sharedViewModel.user.value!!.photoPath,
                        ratingTitle,
                        ratingComment,
                        ratingBar.rating.toString())//,
                        //sharedViewModel.user.value!!.userId)

                    sharedViewModel.putRating(myRating, viewModel.sellerId, viewModel.itemId)

                    Toast.makeText(activity, "Rating saved!", Toast.LENGTH_LONG).show()
                    findNavController().navigate(R.id.action_ratingUserAboutItemFragment_to_nav_boughtItemsListFragment)
                }.setNegativeButton("No"){
                        dialog: DialogInterface, _: Int -> dialog.cancel()
                }.create().show()
                true
            }
            else -> {
                handleBackAlert()
                true
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                handleBackAlert()
            }
        })
    }

    private fun handleBackAlert() {
        AlertDialog.Builder(activity).setTitle("Discard this rating").setMessage("Are you sure?").setPositiveButton("Yes"){
                dialog: DialogInterface, _: Int -> dialog.dismiss()
            findNavController().navigate(R.id.action_ratingUserAboutItemFragment_to_nav_boughtItemsListFragment)
        }.setNegativeButton("No"){
                dialog: DialogInterface, _: Int -> dialog.cancel()
        }.create().show()
    }

}
