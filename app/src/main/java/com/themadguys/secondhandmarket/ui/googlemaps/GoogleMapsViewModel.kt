package com.themadguys.secondhandmarket.ui.googlemaps

import android.location.Location
import androidx.lifecycle.ViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng

class GoogleMapsViewModel : ViewModel() {

    //var placesClient : PlacesClient? = null
    var fusedLocationProviderClient: FusedLocationProviderClient? = null
    var address = ""
    var locationPermissionGranted = false
    var lastKnownLocation : Location? = null
    var myPosition : LatLng? = null
}
