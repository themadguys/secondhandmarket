package com.themadguys.secondhandmarket.ui.editprofile

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.themadguys.secondhandmarket.*
import kotlinx.android.synthetic.main.edit_profile_fragment.*
import java.io.File

class EditProfileFragment : Fragment() {

    private lateinit var sharedViewModel: SharedViewModel
    private lateinit var viewModel : EditProfileViewModel

    @SuppressLint("RestrictedApi")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.edit_profile_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
        registerForContextMenu(camera)
        camera.setOnClickListener{
            Toast.makeText(activity, "Long Press to show options", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModel = activity!!.run { ViewModelProviders.of(this).get(SharedViewModel::class.java) }
        viewModel = ViewModelProviders.of(this).get(EditProfileViewModel::class.java)
        viewModel.chosenPhotoPath.observe(viewLifecycleOwner, Observer { Glide.with(activity!!).asBitmap().load(viewModel.chosenPhotoPath.value).into(photo) })

        val user = sharedViewModel.user.value!!
        if(user.photoPath != "" && viewModel.chosenPhotoPath.value == null)
            viewModel.chosenPhotoPath.value = user.photoPath
        viewModel.initialPhotoPath = viewModel.chosenPhotoPath.value//user.photoPath
        sharedViewModel.userKey = user.userId /*userKey may be useful further*/
        full_name.setText(user.name)
        nickname.setText(user.nickname)
        email.setText(user.email)
        geographic_area.setText(user.country)

        arguments?.let {
            if(arguments!!.containsKey("locationAddress"))
            {
                if(arguments!!.getString("locationAddress") != "")
                    geographic_area.setText(arguments!!.getString("locationAddress")!!)
                email.setText(arguments!!.getString("userEmail")!!)
                nickname.setText(arguments!!.getString("userNickname")!!)
                full_name.setText(arguments!!.getString("userFullName")!!)
                viewModel.chosenPhotoPath.value = arguments!!.getString("userPhoto")!!
            }
        }



        geographic_area.setOnTouchListener { v, event ->
            v.performClick()
            if(event.action == MotionEvent.ACTION_UP) {
                if(event.x >= (geographic_area.width - geographic_area.compoundDrawables[2].bounds.width())) {

                    val bundle = bundleOf("locationAddress" to geographic_area.text.toString())
                    bundle.putString("userFullName",full_name.text.toString())
                    bundle.putString("userNickname",nickname.text.toString())
                    bundle.putString("userEmail",email.text.toString())
                    bundle.putString("userPhoto",viewModel.chosenPhotoPath.value)
                    if(activity!!.currentFocus != null)
                    {
                        val keyboardOut = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        keyboardOut.hideSoftInputFromWindow(activity!!.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                    }
                    findNavController().navigate(R.id.action_editProfileFragment_to_googleMapsFragment, bundle)
                    return@setOnTouchListener true
                }
            }
            return@setOnTouchListener false
        }
    }


    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?){
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = activity!!.menuInflater
        inflater.inflate(R.menu.picture_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.select_from_gallery -> {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
                {
                    if(ContextCompat.checkSelfPermission(activity!!,android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
                    {
                        //denied
                        val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                        //popup activity
                        requestPermissions(permissions,REQUEST_PERMISSION_GALLERY)
                    }
                    else
                    {
                        //already granted
                        pickPhotoFromGallery(this)
                    }
                }
                else {
                    //OS < M
                    pickPhotoFromGallery(this)
                }
                true
            }
            R.id.take_photo -> {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
                {
                    if(ContextCompat.checkSelfPermission(activity!!,android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED)
                    {
                        //denied
                        val permissions = arrayOf(android.Manifest.permission.CAMERA)
                        //popup activity
                        requestPermissions(permissions,REQUEST_PERMISSION_CAMERA)
                    }
                    else
                    {
                        //already granted
                        takePhotoFromCamera("profilePic", this)
                    }
                }
                else {
                    //OS < M
                    takePhotoFromCamera("profilePic", this)
                }
                true
            }
            else -> {
                super.onContextItemSelected(item)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode)
        {
            REQUEST_PERMISSION_GALLERY -> {
                if(grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    //user is agree
                    pickPhotoFromGallery(this)
                }
                else
                {
                    //user is not agree
                    Toast.makeText(activity, "Permission DENIED!", Toast.LENGTH_SHORT).show()
                }
            }
            REQUEST_PERMISSION_CAMERA -> {
                if(grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    //user is agree
                    takePhotoFromCamera("profilePic", this)
                }
                else
                {
                    //user is not agree
                    Toast.makeText(activity, "Permission DENIED!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }





    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_PHOTO && resultCode == Activity.RESULT_OK) {
            Toast.makeText(activity, "Picture SAVED!", Toast.LENGTH_SHORT).show()
            viewModel.chosenPhotoPath.value = Uri.fromFile(File(currentPhotoPath)).toString()
        }
        else if(requestCode == REQUEST_PHOTO_FROM_GALLERY && resultCode == Activity.RESULT_OK)
        {
            val URItmp = data?.data as Uri
            Toast.makeText(activity, "Picture LOADED!", Toast.LENGTH_SHORT).show()
            val photoFile: File? = createImageFile("profilePic", this)
            val fileOut = context!!.contentResolver.openOutputStream(Uri.fromFile(photoFile))
            val fileIn = context!!.contentResolver.openInputStream(URItmp)
            fileIn!!.copyTo(fileOut!!)

            viewModel.chosenPhotoPath.value = Uri.fromFile(File(currentPhotoPath)).toString()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.save_profile, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(activity!!.currentFocus != null)
        {
            val keyboardOut = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboardOut.hideSoftInputFromWindow(activity!!.currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
        return when(item.itemId)
        {
            R.id.nav_showprofile -> {
                if(TextUtils.isEmpty(full_name.text.trim().toString()) || TextUtils.isEmpty(nickname.text.trim().toString()) || TextUtils.isEmpty(email.text.trim().toString()) || TextUtils.isEmpty(geographic_area.text.trim().toString()))
                {
                    Toast.makeText(activity, "Empty field not allowed", Toast.LENGTH_SHORT).show()
                }
                else if(!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches())
                {
                    Toast.makeText(activity, "Email is not valid", Toast.LENGTH_SHORT).show()
                    email.error="Enter a valid mail address"
                }
                else {
                    AlertDialog.Builder(activity).setTitle("Save edit").setMessage("Are you sure?").setPositiveButton("Yes"){
                            dialog: DialogInterface, _: Int -> dialog.dismiss()
                        email.error = null
                        val username = full_name.text.trim().toString()
                        val usernickname = nickname.text.trim().toString()
                        val usermail = email.text.toString()
                        val usercountry = geographic_area.text.trim().toString()

                        if (viewModel.chosenPhotoPath.value != null) {
                            if(viewModel.chosenPhotoPath.value!!.contains("profilePic"))
                                Snackbar.make(view!!, "Uploading profile picture...", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null)
                                    .show()
                            sharedViewModel.user.value = User(sharedViewModel.userKey,viewModel.chosenPhotoPath.value!!,username,usernickname,usermail,usercountry,sharedViewModel.tokenId)
                        } else {
                            sharedViewModel.user.value = User(sharedViewModel.userKey,"",username,usernickname,usermail,usercountry,sharedViewModel.tokenId)
                        }
                        Toast.makeText(activity, "Saving...", Toast.LENGTH_LONG).show()
                        sharedViewModel.putUser()

                        findNavController().navigate(R.id.action_editProfileFragment_to_nav_showprofile)
                    }.setNegativeButton("No"){
                            dialog: DialogInterface, _: Int -> dialog.cancel()
                    }.create().show()

                }
                true
            }
            else -> {
                handleBackAlert()
                true
            }
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                handleBackAlert()
            }
        })
    }

    private fun handleBackAlert() {
        if( viewModel.chosenPhotoPath.value == viewModel.initialPhotoPath &&
            sharedViewModel.user.value!!.name == full_name.text.trim().toString() &&
            sharedViewModel.user.value!!.nickname == nickname.text.trim().toString() &&
            sharedViewModel.user.value!!.email == email.text.trim().toString() &&
            sharedViewModel.user.value!!.country == geographic_area.text.trim().toString() )
            findNavController().navigate(R.id.action_editProfileFragment_to_nav_showprofile)
        else
            AlertDialog.Builder(activity).setTitle("Discard edit").setMessage("Are you sure?").setPositiveButton("Yes"){
                    dialog: DialogInterface, _: Int -> dialog.dismiss()
                findNavController().navigate(R.id.action_editProfileFragment_to_nav_showprofile)
            }.setNegativeButton("No"){
                    dialog: DialogInterface, _: Int -> dialog.cancel()
            }.create().show()
    }


}
