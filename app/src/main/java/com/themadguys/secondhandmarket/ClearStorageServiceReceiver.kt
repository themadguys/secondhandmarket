package com.themadguys.secondhandmarket

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.util.Log

class ClearStorageServiceReceiver : BroadcastReceiver() {

    //@SuppressLint("UnsafeProtectedBroadcastReceiver") /*spoofed intent handling ignored*/
    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d("ClearStorage", "Entering BroadcastReceiver")
        intent?.let {
            if(intent.action.equals(Intent.ACTION_BOOT_COMPLETED) ||
                intent.action.equals(Intent.ACTION_REBOOT) ||
                intent.action.equals(Intent.ACTION_PACKAGE_FIRST_LAUNCH)) {
            /*handle clearStorage service for background daily schedule*/
                val componentService = ComponentName(context!!, ClearStorageService::class.java)
                val info = JobInfo.Builder(4242, componentService) /*first is the ID*/
                    //.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED) /*only if network in ON*/
                    .setPersisted(true) /*persist on boot*/ /*not sure if needed since broadcast receiver does the same*/
                    .setPeriodic(24 * 60 * 60 * 1000) /*once a day*/
                    .build()
                val scheduler = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
                val resultCode = scheduler.schedule(info)
                if(resultCode == JobScheduler.RESULT_SUCCESS)
                    Log.d("ClearStorage", "<Job scheduled>")
                else
                    Log.d("ClearStorage", "<Job scheduling failed>")
            }
        }
    }

}