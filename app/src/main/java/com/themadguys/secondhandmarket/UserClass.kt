package com.themadguys.secondhandmarket

import kotlinx.serialization.Serializable

@Serializable
data class User(val userId: String = "profileNew",
                var photoPath: String = "",
                val name:String = "Paolo Rossi",
                val nickname:String = "your nickname",
                val email:String = "paolo.rossi@gmail.com",
                val country:String = "Italy",
                var tokenId:String = "FCMtoken")