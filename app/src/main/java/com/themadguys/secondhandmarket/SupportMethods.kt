package com.themadguys.secondhandmarket

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


const val REQUEST_PHOTO = 1
const val REQUEST_PHOTO_FROM_GALLERY = 2
const val REQUEST_PERMISSION_GALLERY = 3
const val REQUEST_PERMISSION_CAMERA = 4
const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5

var currentPhotoPath = ""

fun pickPhotoFromGallery(fragment: Fragment) {

    val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    intent.type = "image/*"
    Log.d("kkk", "sending request to get data from gallery")
    fragment.startActivityForResult(intent, REQUEST_PHOTO_FROM_GALLERY)
}

fun takePhotoFromCamera(key: String, fragment: Fragment) {
    Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intent ->
        intent.resolveActivity(fragment.activity!!.packageManager)?.also {
            val photoFile: File? = try {
                createImageFile(key, fragment)
            } catch (e: IOException) {
                Log.e("kkk", "error creating photo file")
                null
            }
            Log.d("kkk", "created photo file: $photoFile")
            photoFile?.also {
                Log.d("kkk", "creating photo URI...")
                val photoUri: Uri = FileProvider.getUriForFile(
                    fragment.activity!!,
                    "com.themadguys.secondhandmarket.fileprovider",
                    it
                )
                Log.d("kkk", "adding URI to intent: $photoUri")
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                Log.d("kkk", "sending to camera: ${intent.extras}")
                fragment.startActivityForResult(intent, REQUEST_PHOTO)
            }
        }
    }
}

@SuppressLint("SimpleDateFormat")
@Throws(IOException::class)
fun createImageFile(key: String, fragment: Fragment): File {
    // Create an image file name
    val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val storageDir: File = fragment.activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
    Log.d("kkk", "creating photo file in $storageDir")
    return File.createTempFile(
        key + "_" + timeStamp, /* prefix */
        ".jpg", /* suffix */
        storageDir /* directory */
    ).apply {

        currentPhotoPath = absolutePath
    }
}

fun clearStorageSvc(dir: File) {
    /*called in  a background service*/

    for (file in dir.listFiles()!!) /*remove all photo files to clean memory*/
        file!!.delete()

    Log.d("ClearStorage", "pics now in dir: ${dir.listFiles()!!.size}")

}