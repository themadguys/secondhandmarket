package com.themadguys.secondhandmarket

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.themadguys.secondhandmarket.ui.itemdetails.ItemDetailsFragment
import com.themadguys.secondhandmarket.ui.itemedit.ItemEditFragment
import kotlinx.android.synthetic.main.user_card.view.*

class InterestedUsersAdapter(private val parentFragment: Fragment, private var selectedPos : Int) : RecyclerView.Adapter<InterestedUsersAdapter.MyViewHolder>() {
    class MyViewHolder(viewItem: View) : RecyclerView.ViewHolder(viewItem){
        val userphoto = viewItem.header_userpic
        //val usercard = viewItem.itemCard
    }

    private var myDataset = ArrayList<User>()
    //private var selectedPos = -1
    private lateinit var mCallback : OnItemClickListener

    fun setOnItemClickListener(callback : OnItemClickListener){
        mCallback = callback
    }

    fun setDataset(data : ArrayList<User>){

        myDataset.clear()
        myDataset.addAll(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val viewItem = LayoutInflater.from(parent.context).inflate(R.layout.user_card, parent, false)
        return MyViewHolder(viewItem)
    }

    override fun getItemCount(): Int {
        return myDataset.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.userphoto.setImageResource(R.drawable.ic_person_black_24dp)
        if(myDataset[position].photoPath != "")
            Glide.with(parentFragment.activity!!).load(myDataset[position].photoPath)
                .circleCrop().into(holder.userphoto)

        if(parentFragment is ItemDetailsFragment)
        {
            holder.itemView.setOnClickListener {
               mCallback.onClick(myDataset[position], position, holder.itemView)
            }
        }
        else if(parentFragment is ItemEditFragment)
        {
            if(selectedPos == position)
                holder.userphoto.alpha = 0.7f
            else
                holder.userphoto.alpha = 1f

            holder.itemView.setOnClickListener {
                if(selectedPos == position)
                {
                    selectedPos = -1
                    notifyItemChanged(position)
                    mCallback.onClick(null, selectedPos, holder.itemView)
                }
                else
                {
                    notifyItemChanged(selectedPos)
                    selectedPos = position//holder.adapterPosition
                    notifyItemChanged(selectedPos)
                    /*send callback to fragment*/
                    mCallback.onClick(myDataset[position], selectedPos, holder.itemView)
                }
            }
        }
    }

    interface OnItemClickListener{
        fun onClick(user: User?, position: Int, itemView: View)
    }
}